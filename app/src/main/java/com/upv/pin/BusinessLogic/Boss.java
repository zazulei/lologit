package com.upv.pin.BusinessLogic;

import android.media.Image;

/**
 * Created by mifercre on 12/11/14.
 */
public class Boss {

    private String email;
    private int id;
    private Image image;
    private String imageURL;
    private String name;
    private String phone;
    private String surname;
    private String password;
    private int idLocal;

    public Boss() { }

    public Boss(String email, int id, Image image, String imageURL, String name, String phone, int idLocal, String password, String surname) {
        this.email = email;
        this.id = id;
        this.image = image;
        this.imageURL = imageURL;
        this.name = name;
        this.phone = phone;
        this.idLocal = idLocal;
        this.password = password;
        this.surname = surname;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
