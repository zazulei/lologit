package com.upv.pin.BusinessLogic;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


/**
 * Created by javat-0 on 30/09/2014.
 */
public class Local implements Parcelable {

    private int categoryID;
    private String categoryName;
    private String categoryImage;
    private int price;
    private int rating;
    private int numRatings;
    private int id;
    private int maxAssistants;

    private double latitude;
    private double longitude;

    private String image_url;
    private String address;
    private String description;
    private String email;
    private String name;
    private String phoneNumber;
    private String schedule;

    private List<Event> events;
    private List<Offer> offers;

    private int bossId;
    private String bossEmail;

    public Local(){}

    public Local(String address, String description, int id, String image, double latitude, double longitude, String name, String phoneNumber, int maxAssistants, int numRatings) {
        this.address = address;
        this.description = description;
        this.id = id;
        this.image_url = image;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.maxAssistants = maxAssistants;
        this.numRatings = numRatings;
    }

    public Local(Parcel in){
        categoryID = in.readInt();
        price = in.readInt();
        rating = in.readInt();
        id = in.readInt();

        latitude = in.readDouble();
        longitude = in.readDouble();

        image_url = in.readString();
        address = in.readString();
        description = in.readString();
        email = in.readString();
        name = in.readString();
        phoneNumber = in.readString();
        schedule = in.readString();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void addEvent(Event e){
        events.add(e);
    }
    public Event getEvent(int id){
        return events.get(id);
    }

    public Event removeEvent(int id){
        return events.remove(id);
    }
    public void addOffer(Offer o){
        offers.add(o);
    }
    public Offer getOffer(int id){
        return offers.get(id);
    }

    public Offer removeOffer(int id){
        return offers.remove(id);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image_url;
    }

    public void setImage(String image_url) {
        this.image_url = image_url;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public int getBossId() {
        return bossId;
    }

    public void setBossId(int bossId) {
        this.bossId = bossId;
    }

    public String getBossEmail() {
        return bossEmail;
    }

    public void setBossEmail(String bossEmail) {
        this.bossEmail = bossEmail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(price);
        parcel.writeInt(rating);
        parcel.writeInt(categoryID);

        parcel.writeString(image_url);
        parcel.writeString(address);
        parcel.writeString(description);
        parcel.writeString(email);
        parcel.writeString(name);
        parcel.writeString(phoneNumber);
        parcel.writeString(schedule);

        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
    }

    public int getMaxAssistants() {
        return maxAssistants;
    }

    public void setMaxAssistants(int maxAssistants) {
        this.maxAssistants = maxAssistants;
    }

    public int getNumRatings() {
        return numRatings;
    }

    public void setNumRatings(int numRatings) {
        this.numRatings = numRatings;
    }
}
