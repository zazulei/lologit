package com.upv.pin.BusinessLogic;


import android.media.Image;

/**
 * Created by javat-0 on 30/09/2014.
 */
public class User {
    private String email;
    private int id;
    private Image image;
    private String imageURL;
    private String name;
    private String phone;
    private String surname;
    private String password;
    private boolean isSignedUp;

    public User() { }

    public User(String email, int id, Image image, String name, String phone, String surname) {
        this.email = email;
        this.id = id;
        this.image = image;
        this.name = name;
        this.phone = phone;
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSignedUp() {
        return isSignedUp;
    }

    public void setSignedUp(boolean isSignedUp) {
        this.isSignedUp = isSignedUp;
    }
}
