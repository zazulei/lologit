package com.upv.pin.BusinessLogic;

/**
 * Created by mifercre on 09/12/14.
 */
public class UserPreferences {

    private int price;
    private int rating;
    private String distancia;
    private int idUser;
    private int idCategory;

    public UserPreferences(int idCategory, int idUser, String distancia, int rating, int price) {
        this.idCategory = idCategory;
        this.idUser = idUser;
        this.distancia = distancia;
        this.rating = rating;
        this.price = price;
    }

    public UserPreferences() {}

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
