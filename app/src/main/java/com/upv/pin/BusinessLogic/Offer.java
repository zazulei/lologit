package com.upv.pin.BusinessLogic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by javat-0 on 30/09/2014.
 */
public class Offer {
    private Date date;
    private String description;
    private int id;
    //local where the offer is held
    private Local local;
    private String image;
    private String title;

    public Offer(){}

    public Offer(Date date, String description, int id, Local local, String title, String image) {
        this.date = date;
        this.description = description;
        this.id = id;
        this.local = local;
        this.image = image;
        this.setTitle(title);
    }
 

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public void setDate(String str){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            str = str.substring(0,str.indexOf("+"));
           date = dateFormat.parse(str);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
