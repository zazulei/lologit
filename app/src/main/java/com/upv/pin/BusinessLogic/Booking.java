package com.upv.pin.BusinessLogic;

/**
 * Created by mifercre on 26/11/14.
 */
public class Booking {

    private int userId;
    private int localId;
    private int numAssistants;
    private long dateLong;

    public Booking(int userId, int localId, int numAssistants, long dateLong) {
        this.userId = userId;
        this.localId = localId;
        this.numAssistants = numAssistants;
        this.dateLong = dateLong;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public long getDateLong() {
        return dateLong;
    }

    public void setDateLong(long dateLong) {
        this.dateLong = dateLong;
    }

    public int getNumAssistants() {
        return numAssistants;
    }

    public void setNumAssistants(int numAssistants) {
        this.numAssistants = numAssistants;
    }
}
