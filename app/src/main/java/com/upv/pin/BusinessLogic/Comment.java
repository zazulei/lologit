package com.upv.pin.BusinessLogic;

/**
 * Created by mifercre on 26/11/14.
 */
public class Comment {

    private String commentText;
    private int userId;
    private int localId;
    private String userName;

    public Comment(String commentText, int userId, int localId) {
        this.commentText = commentText;
        this.userId = userId;
        this.localId = localId;
    }

    public Comment() {}

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
