package com.upv.pin.BusinessLogic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by javat-0 on 30/09/2014.
 */
public class LocalList {
    private List<Local> locals;

    public LocalList(List<Local> locals) {
        this.locals = locals;
    }

    public LocalList() {
        locals = new ArrayList<Local>();
    }

    public void add(Local l){
        locals.add(l);
    }

    public Local remove(int id){
       return locals.remove(id);
    }

    public Local get(int id){
        return locals.get(id);
    }

    public int size(){return locals.size();}

}
