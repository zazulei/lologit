package com.upv.pin.lolo;

import android.util.Log;
import android.util.Xml;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Comment;
import com.upv.pin.BusinessLogic.Event;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;
import com.upv.pin.BusinessLogic.Offer;
import com.upv.pin.BusinessLogic.User;
import com.upv.pin.BusinessLogic.UserPreferences;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Created by javat-0 on 07/10/2014.
 */
public class XMLParser {

    public static int getEntitiesCount(BufferedInputStream conn) {
        try {
            return conn.read();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return -1;

    }

    public static LocalList getLocals(BufferedInputStream stream)throws IOException{
        LocalList locals = new LocalList();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            Local currentLocal = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("locals")) {
                            currentLocal = new Local();
                        } else if (currentLocal != null){
                            if (name.equalsIgnoreCase("localaddress")) {
                                currentLocal.setAddress(parser.nextText());
                                //Log.d("address", currentLocal.getAddress());
                            } else if (name.equalsIgnoreCase("localidcategory")) {
                                parser.nextTag();
                                if(parser.getName().equalsIgnoreCase("categoryId")){
                                    currentLocal.setCategoryID(Integer.parseInt(parser.nextText()));
                                    //System.out.println(currentLocal.getCategoryID()+"");
                                }
                                parser.nextTag();
                                if(parser.getName().equalsIgnoreCase("categoryImage")){
                                    currentLocal.setCategoryImage(parser.nextText());
                                    //System.out.println(currentLocal.getCategoryImage());
                                }
                                parser.nextTag();
                                if(parser.getName().equalsIgnoreCase("categoryName")){
                                    currentLocal.setCategoryName(parser.nextText());
                                    //System.out.println(currentLocal.getCategoryName());
                                }
                                //Log.d("categoryID",currentLocal.getCategoryID() + "");
                            }else if (name.equalsIgnoreCase("localdescription")) {
                                currentLocal.setDescription(parser.nextText());
                                //Log.d("desc",currentLocal.getDescription());
                            }else if (name.equalsIgnoreCase("localemail")) {
                                currentLocal.setEmail(parser.nextText());
                                //Log.d("email",currentLocal.getEmail());
                            }else if (name.equalsIgnoreCase("localid")) {
                                currentLocal.setId(Integer.parseInt(parser.nextText()));
                                //Log.d("id",currentLocal.getId() + "");
                            }else if (name.equalsIgnoreCase("localimage")) {
                                currentLocal.setImage(parser.nextText());
                                //Log.d("image",currentLocal.getImage());
                            }else if (name.equalsIgnoreCase("locallatitude")) {
                                currentLocal.setLatitude(Double.parseDouble(parser.nextText()));
                                //Log.d("attitude",currentLocal.getLatitude() + "");
                            }else if (name.equalsIgnoreCase("locallongitude")) {
                                currentLocal.setLongitude(Double.parseDouble(parser.nextText()));
                                //Log.d("longitude",currentLocal.getLongitude() + "");
                            }else if (name.equalsIgnoreCase("localMaxAssistants")) {
                                currentLocal.setMaxAssistants(Integer.parseInt(parser.nextText()));
                            }else if (name.equalsIgnoreCase("localname")) {
                                currentLocal.setName(parser.nextText());
                                //Log.d("name",currentLocal.getName());
                            }else if (name.equalsIgnoreCase("localphone")) {
                                currentLocal.setPhoneNumber(parser.nextText());
                                //Log.d("phone",currentLocal.getPhoneNumber());
                            }else if (name.equalsIgnoreCase("localprice")) {
                                currentLocal.setPrice(Integer.parseInt(parser.nextText()));
                                //Log.d("price",currentLocal.getPrice() + "");
                            }else if (name.equalsIgnoreCase("localrating")) {
                                currentLocal.setRating(Integer.parseInt(parser.nextText()));
                                //Log.d("rating",currentLocal.getRating() + "");
                            }else if (name.equalsIgnoreCase("localNumRatings")) {
                                currentLocal.setNumRatings(Integer.parseInt(parser.nextText()));
                            }else if (name.equalsIgnoreCase("localschedule")) {
                                currentLocal.setSchedule(parser.nextText());
                                //Log.d("schedule",currentLocal.getSchedule());
                            }else if (name.equalsIgnoreCase("bossId")) {
                                currentLocal.setBossId(Integer.parseInt(parser.nextText()));
                            }else if (name.equalsIgnoreCase("bossEmail")) {
                                currentLocal.setBossEmail(parser.nextText());
                        }

                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("locals") && currentLocal != null) {
                            locals.add(currentLocal);
                        }
                }
                eventType = parser.next();
        }

        } catch (XmlPullParserException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
        return locals;
    }

    public static List<Event> getEvents(BufferedInputStream stream)throws IOException{
        List<Event> events = new ArrayList<Event>();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            Event currentEvent = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("events")) {
                            currentEvent = new Event();
                        } else if (currentEvent != null){
                            if (name.equalsIgnoreCase("eventDate")) {

                                currentEvent.setDate(parser.nextText());
                                Log.d("address", currentEvent.getDate().toString());
                            }else if(name.equalsIgnoreCase("eventDescription")){
                             currentEvent.setDescription(parser.nextText());
                            }else if(name.equalsIgnoreCase("eventId")){
                                currentEvent.setId(Integer.parseInt(parser.nextText()));
                            } else if (name.equalsIgnoreCase("eventIdLocal")) {
                                //Log.d("categoryID",currentEvent.getCategoryID() + "");
                            }else if (name.equalsIgnoreCase("eventImage")) {
                                currentEvent.setImage(parser.nextText());
                                //Log.d("desc",currentEvent.getImage());
                            }else if (name.equalsIgnoreCase("eventTitle")) {
                                currentEvent.setTitle(parser.nextText());
                                //Log.d("email",currentEvent.getEmail());
                            }


                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("events") && currentEvent != null) {
                            events.add(currentEvent);
                        }
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return events;

    }
    public static List<Offer> getOffers(BufferedInputStream stream)throws IOException{

        List<Offer> offers = new ArrayList<Offer>();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            Offer currentOffer = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;

                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("offers")) {
                            Log.i("Bucle", "Entrada");
                            currentOffer = new Offer();
                        }  else if (currentOffer != null){
                            if (name.equalsIgnoreCase("offerDate")) {

                                currentOffer.setDate(parser.nextText());

                               // Log.d("date", currentOffer.getDate().toString());
                            }else if(name.equalsIgnoreCase("offerDescription")){
                                currentOffer.setDescription(parser.nextText());
                            }else if(name.equalsIgnoreCase("offerId")){
                                currentOffer.setId(Integer.parseInt(parser.nextText()));
                            } else if (name.equalsIgnoreCase("offerIdLocal")) {
                            }else if (name.equalsIgnoreCase("offerImage")) {
                                currentOffer.setImage(parser.nextText());
                                //Log.d("desc",currentOffer.getImage());
                            }else if (name.equalsIgnoreCase("offerTitle")) {
                                currentOffer.setTitle(parser.nextText());

                            }


                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("offers") && currentOffer != null) {
                            offers.add(currentOffer);
                        }
                }
                //showOffers(offers);
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //showOffers(offers);
        return offers;
    }



    public static StringWriter localToXML(Local l){
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try{
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8",true);

            //<locals>
            serializer.startTag("","locals");

            serializer.startTag("","localAddress");
            serializer.text( l.getAddress());
            serializer.endTag("","localAddress");

            serializer.startTag("","localDescription");
            serializer.text(l.getDescription());
            serializer.endTag("", "localDescription");

            serializer.startTag("","localEmail");
            serializer.text( l.getEmail());
            serializer.endTag("","localEmail");

            serializer.startTag("","localId");
            serializer.text("" + l.getId());
            serializer.endTag("", "localId");

            serializer.startTag("","localImage");
            serializer.text( l.getImage());
            serializer.endTag("","localImage");

            serializer.startTag("","localLatitude");
            serializer.text("" + l.getLatitude());
            serializer.endTag("", "localLatitude");

            serializer.startTag("","localLongitude");
            serializer.text( ""+l.getLongitude());
            serializer.endTag("","localLongitude");

            serializer.startTag("","localName");
            serializer.text( ""+l.getName());
            serializer.endTag("","localName");

            serializer.startTag("","localPhone");
            serializer.text( ""+l.getPhoneNumber());
            serializer.endTag("","localPhone");

            serializer.startTag("","localPrice");
            serializer.text( ""+l.getPrice());
            serializer.endTag("","localPrice");

            serializer.startTag("","localRating");
            serializer.text( ""+l.getRating());
            serializer.endTag("","localRating");

            serializer.startTag("","localSchedule");
            serializer.text( ""+l.getSchedule());
            serializer.endTag("","localSchedule");


            //</locals>
            serializer.endTag("", "locals");
            writer.close();
            serializer.endDocument();
            Log.d("XMLParser","Locales:"+ writer);

            return writer;
        }

        catch(Exception e){
            throw new RuntimeException(e);

        }

    }

    public static List<User> getUsers(BufferedInputStream stream) throws IOException {
        List<User> users = new ArrayList<User>();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            User currentUser = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("users")) {
                            currentUser = new User();
                        } else if (currentUser != null){
                            if (name.equalsIgnoreCase("userEmail")) {
                                currentUser.setEmail(parser.nextText());
                                Log.d("email", currentUser.getEmail());
                            }else if(name.equalsIgnoreCase("userId")){
                                currentUser.setId(Integer.parseInt(parser.nextText()));
                            }else if(name.equalsIgnoreCase("userImage")){
                                currentUser.setImageURL(parser.nextText());
                            } else if (name.equalsIgnoreCase("userName")) {
                                currentUser.setName(parser.nextText());
                            }else if (name.equalsIgnoreCase("userPassword")) {
                                currentUser.setPassword(parser.nextText());
                            }else if (name.equalsIgnoreCase("userPhone")) {
                                currentUser.setPhone(parser.nextText());
                            }else if (name.equalsIgnoreCase("userSurname")) {
                                currentUser.setSurname(parser.nextText());
                            }

                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("users") && currentUser != null) {
                            users.add(currentUser);
                        }
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return users;
    }

    public static List<Boss> getBosses(BufferedInputStream stream) throws IOException {
        List<Boss> bosses = new ArrayList<Boss>();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            Boss currentBoss = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;

                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("bosses")) {
                            currentBoss = new Boss();
                        } else if (currentBoss != null){
                            if (name.equalsIgnoreCase("bossEmail")) {
                                currentBoss.setEmail(parser.nextText());
                                Log.d("email", currentBoss.getEmail());
                            }else if(name.equalsIgnoreCase("bossId")){
                                currentBoss.setId(Integer.parseInt(parser.nextText()));
                            }else if(name.equalsIgnoreCase("localId")){
                                currentBoss.setIdLocal(Integer.parseInt(parser.nextText()));
                            }else if(name.equalsIgnoreCase("bossId")){
                                currentBoss.setId(Integer.parseInt(parser.nextText()));
                            }else if(name.equalsIgnoreCase("bossImage")){
                                currentBoss.setImageURL(parser.nextText());
                            } else if (name.equalsIgnoreCase("bossName")) {
                                currentBoss.setName(parser.nextText());
                            }else if (name.equalsIgnoreCase("userPassword")) {
                                currentBoss.setPassword(parser.nextText());
                            }else if (name.equalsIgnoreCase("userPhone")) {
                                currentBoss.setPhone(parser.nextText());
                            }else if (name.equalsIgnoreCase("userSurname")) {
                                currentBoss.setSurname(parser.nextText());
                            }

                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("bosses") && currentBoss != null) {
                            bosses.add(currentBoss);
                        }
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return bosses;
    }

    public static UserPreferences getUserPreferences(BufferedInputStream stream) throws IOException {
        UserPreferences pref = new UserPreferences();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;

                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("preferences")) {
                            pref = new UserPreferences();
                        } else if (pref != null){
                            if (name.equalsIgnoreCase("preferenceDistance")) {
                                pref.setDistancia(parser.nextText());
                            }else if(name.equalsIgnoreCase("preferenceIdCategory")){
                                parser.nextTag();
                                parser.nextTag();
                                if(parser.getName().equalsIgnoreCase("categoryId")){
                                    pref.setIdCategory(Integer.parseInt(parser.nextText()));
                                }
                            }else if(name.equalsIgnoreCase("preferenceIdUser")){
                                parser.nextTag();
                                if(parser.getName().equalsIgnoreCase("userId")){
                                    pref.setIdUser(Integer.parseInt(parser.nextText()));
                                }
                            }else if(name.equalsIgnoreCase("preferencePrice")){
                                pref.setPrice(Integer.parseInt(parser.nextText()));
                            }else if(name.equalsIgnoreCase("preferenceRating")) {
                                pref.setRating(Integer.parseInt(parser.nextText()));
                            }
                        }
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return pref;
    }

    public static List<Comment> getComments(BufferedInputStream stream) throws IOException {
        ArrayList<Comment> comments = new ArrayList<Comment>();
        Comment comment = new Comment();

        try {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

            parser.setInput(stream, null);

            int eventType = parser.getEventType(); //get the state of the XML
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name;

                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: //Initialization
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName(); //get Tag Name
                        if (name.equalsIgnoreCase("comments")) {
                            comment = new Comment();
                        } else if (comment != null){
                            if (name.equalsIgnoreCase("localId")) {
                                /*while(parser.getName() != null && !parser.getName().equalsIgnoreCase("localId")) {
                                    parser.next();
                                }*/
                                //if(parser.getName().equalsIgnoreCase("localId")){
                                    comment.setLocalId(Integer.parseInt(parser.nextText()));
                                //}
                            }else if(name.equalsIgnoreCase("userId")) {
                                /*while(!parser.getName().equals("userId")) {
                                    parser.next();
                                }*/
                                //if(parser.getName().equalsIgnoreCase("userId")){
                                comment.setUserId(Integer.parseInt(parser.nextText()));
                                //}
                            }else if(name.equalsIgnoreCase("userName")) {
                                comment.setUserName(parser.nextText());
                            }else if(name.equalsIgnoreCase("commentText")){
                                comment.setCommentText(parser.nextText());
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("comments") && comment != null) {
                            comments.add(comment);
                        }
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return comments;
    }
}
