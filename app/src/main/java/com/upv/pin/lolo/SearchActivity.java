package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.ExpandableListAdapter;
import com.upv.pin.BusinessLogic.Local;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SearchActivity extends Activity implements View.OnClickListener {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    String busqueda="noValue", categoria="noValue", distancia="noValue", precio="noValue", valoracion="noValue";
    Button search, where;
    EditText editMessage;
    TextView categoriaElegida, distanciaElegida, precioElegido, valoracionElegida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        editMessage = (EditText) findViewById(R.id.edit_message);
        categoriaElegida = (TextView) findViewById(R.id.textViewCategoriaElegida);
        categoriaElegida.setOnClickListener(this);
        distanciaElegida = (TextView) findViewById(R.id.textViewDistanciaElegida);
        distanciaElegida.setOnClickListener(this);
        precioElegido = (TextView) findViewById(R.id.textViewPrecioElegida);
        precioElegido.setOnClickListener(this);
        valoracionElegida = (TextView) findViewById(R.id.textViewValoracionElegida);
        valoracionElegida.setOnClickListener(this);

        search = (Button) findViewById(R.id.ButtonSearch);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SearchActivity.this, LocalListActivity.class);
                intent.putExtra("method", "findBySearch");
                if(editMessage.getText().toString() != null && !editMessage.getText().toString().trim().equals("")) {
                    busqueda = editMessage.getText().toString();
                } else busqueda = "noValue";

                intent.putExtra("keyword", busqueda);
                intent.putExtra("distance", distancia);
                intent.putExtra("category",categoria);
                intent.putExtra("price",price(precio));
                intent.putExtra("rating", rating(valoracion));
                System.out.println("keyword: " + busqueda + "categoria: " + categoria + "distancia: " + distancia + "precio: " + price(precio) + "valoracion: " + rating(valoracion));
                if(busqueda.equals("noValue") && distancia.equals("noValue") && categoria.equals("noValue") && precio.equals("noValue") && valoracion.equals("noValue")) {
                    Toast.makeText(getApplicationContext(), "No has seleccionado ningun filtro", Toast.LENGTH_SHORT).show();
                } else startActivity(intent);
            }

        });
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);


        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                String posicion = listDataHeader.get(groupPosition);
                //Intent intent = new Intent(SearchActivity.this, LocalListActivity.class);
                if(posicion.equals("Categoría")) {

                    categoria = listDataChild.get(
                            listDataHeader.get(groupPosition)).get(
                            childPosition);
                    categoriaElegida.setText(categoria);
                    categoriaElegida.setVisibility(View.VISIBLE);
                    //ntent.putExtra("method", "findByCategory");
                    //intent.putExtra("category", categoria+"s");

                }
                else if (posicion.equals("Distancia")) {
                    distancia=listDataChild.get(
                            listDataHeader.get(groupPosition)).get(
                            childPosition);
                    distanciaElegida.setText(distancia);
                    distanciaElegida.setVisibility(View.VISIBLE);
                }
                else if (posicion.equals("Precio")) {
                    precio=listDataChild.get(
                            listDataHeader.get(groupPosition)).get(
                            childPosition);
                    precioElegido.setText(precio);
                    precioElegido.setVisibility(View.VISIBLE);
                }
                else if (posicion.equals("Valoración Minima")) {
                    valoracion=listDataChild.get(
                            listDataHeader.get(groupPosition)).get(
                            childPosition);
                    valoracionElegida.setText(valoracion);
                    valoracionElegida.setVisibility(View.VISIBLE);
                }

                //System.out.println("categoria: "+categoria+"distancia: "+distancia+"precio: "+precio+"valoracion: "+valoracion);

                //startActivity(intent);
                return false;
            }
        });
    }

    private int rating (String r){
        if (r.equals("1/5")) return 1;
        else if (r.equals("2/5")) return 2;
        else if (r.equals("3/5")) return 3;
        else if (r.equals("4/5")) return 4;
        else if (r.equals("5/5"))return 5;
        return -1;
    }

    private int price(String p) {
        if (p.equals("€")) return 1;
        else if (p.equals("€€")) return 2;
        else if (p.equals("€€€")) return 3;
        return -1;
    }
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Categoría");
        listDataHeader.add("Distancia");
        listDataHeader.add("Precio");
        listDataHeader.add("Valoración Minima");

        // Adding child data
        List<String> cat = new ArrayList<String>();
        //cat.add("Cervecerías");
        cat.add("Discotecas");
        cat.add("Cines");
        //cat.add("Cafeterías");
        cat.add("Restaurantes");
        cat.add("Pubs");
        cat.add("Hoteles");
        cat.add("Teatros");
        cat.add("Bares");

        List<String> dist = new ArrayList<String>();
        dist.add("Cerca(1km)");
        dist.add("Zona(5km)");
        dist.add("Ciudad(10km)");

        List<String> pre = new ArrayList<String>();
        pre.add("€");
        pre.add("€€");
        pre.add("€€€");

        List<String> val = new ArrayList<String>();
        val.add("1/5");
        val.add("2/5");
        val.add("3/5");
        val.add("4/5");
        val.add("5/5");

        listDataChild.put(listDataHeader.get(0), cat); // Header, Child data
        listDataChild.put(listDataHeader.get(1), dist);
        listDataChild.put(listDataHeader.get(2), pre);
        listDataChild.put(listDataHeader.get(3), val);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewCategoriaElegida:
                categoria = "noValue";
                v.setVisibility(View.GONE);
                break;
            case R.id.textViewDistanciaElegida:
                distancia = "noValue";
                v.setVisibility(View.GONE);
                break;
            case R.id.textViewPrecioElegida:
                precio = "noValue";
                v.setVisibility(View.GONE);
                break;
            case R.id.textViewValoracionElegida:
                valoracion = "noValue";
                v.setVisibility(View.GONE);
                break;
        }
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
}
