package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import com.upv.pin.BusinessLogic.Event;
import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.User;

import java.io.IOException;


public class LocalManagementActivity extends Activity {

    private String categoria;
    private Button bimage;
    private Bitmap SelectedImage;
    private EditText _name,_email, _address, _schelude, _description, _phone;
    private String filePath = "";
    private ImageView _image;
    private RadioButton r1,r2,r3,r4,r5,r6,r7,rb;
    private RadioGroup rg;

    private GlobalState state;
    private Boss boss;
    private Local local;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_management);
        bimage = (Button) findViewById(R.id.Button_Imagen);

        state = (GlobalState) getApplicationContext();
        boss = state.getBoss();


        local = new Local();
        local.setId(getIntent().getIntExtra("LocalID", 0));
        local.setName(getIntent().getStringExtra("LocalName"));
        local.setEmail(getIntent().getStringExtra("Email"));
        local.setAddress(getIntent().getStringExtra("LocalAddress"));
        local.setRating(getIntent().getIntExtra("LocalRating", 0));
        local.setNumRatings(getIntent().getIntExtra("LocalRatingNumber", 0));
        local.setPrice(getIntent().getIntExtra("LocalPrice", 0));
        local.setImage(getIntent().getStringExtra("LocalImage"));
        local.setCategoryID(getIntent().getIntExtra("LocalCategory", 0));
        local.setCategoryName(getIntent().getStringExtra("LocalCategoryName"));
        local.setDescription(getIntent().getStringExtra("LocalDescription"));
        local.setPhoneNumber(getIntent().getStringExtra("LocalPhone"));
        local.setSchedule(getIntent().getStringExtra("LocalSchedule"));
        local.setLongitude(getIntent().getDoubleExtra("LocalLongitud",0));
        local.setLatitude((getIntent().getDoubleExtra("LocalLatitud",0)));
        filePath = local.getImage();


        _name = (EditText) findViewById(R.id.edit_name);
        _email = (EditText) findViewById(R.id.edit_email);
        _address = (EditText) findViewById(R.id.edit_address);
        _schelude = (EditText) findViewById(R.id.edit_schelude);
        _description = (EditText) findViewById(R.id.edit_description);
        _phone = (EditText) findViewById(R.id.edit_phone);
        _image = (ImageView) findViewById(R.id.local_management_image);

        cambiarDatosEnLaUI(local);

        getActionBar().setTitle("Gestión del local");
    }



    private void cambiarDatosEnLaUI(Local l) {
        _name.setText(l.getName());
        _email.setText(l.getEmail());
        _address.setText(l.getAddress());
        _phone.setText(l.getPhoneNumber());
        _description.setText(l.getDescription());
        _schelude.setText(l.getSchedule());

        if(l.getImage() != null) {
            System.out.println(l.getImage());
            if(l.getImage().contains("/storage/emulated/")) {
                _image.setImageDrawable(Drawable.createFromPath(l.getImage()));
                filePath = l.getImage();
            }
            else {
                Picasso.with(state).load(l.getImage()).into(_image);
                local.setImage(l.getImage());
            }
        }

    }

    public void updateLocal(View v){


        if (_name.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Introduce un nombre",
                    Toast.LENGTH_LONG).show();
        }
        else if (_address.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), "Introduce una dirección",
                    Toast.LENGTH_LONG).show();
        }
        else {
            local.setName(_name.getText().toString().trim());
            local.setAddress(_address.getText().toString().trim());
            local.setSchedule(_schelude.getText().toString().trim());
            local.setPhoneNumber(_phone.getText().toString().trim());
            local.setEmail(_email.getText().toString().trim());
            local.setDescription(_description.getText().toString().trim());
            local.setImage(filePath);

            /*rg = (RadioGroup) findViewById(R.id.rgroup);

            int selectedID = rg.getCheckedRadioButtonId();
            rb = (RadioButton) findViewById(selectedID);
            local.setCategoryName(rb.getText().toString());
            System.out.println(rb.getText().toString());*/

            Update up = new Update();
            up.execute(local);
        }
    }

    private class Update extends AsyncTask<Local, Void, String> {

        @Override
        protected String doInBackground(Local... params) {
            try {
                return WSController.updateLocal(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            goToLocalProfile();
        }
    }

    public void goToLocalProfile() {
        Local temp = local;

        Intent intent = new Intent(LocalManagementActivity.this, LocalProfileActivity.class);
        intent.putExtra("LocalID", temp.getId());
        intent.putExtra("LocalName", temp.getName());
        intent.putExtra("LocalAddress", temp.getAddress());
        intent.putExtra("LocalRating", temp.getRating());
        intent.putExtra("LocalPrice", temp.getPrice());
        intent.putExtra("LocalImage", filePath);
        intent.putExtra("LocalCategory", temp.getCategoryID());
        intent.putExtra("LocalCategoryName", temp.getCategoryName());
        intent.putExtra("LocalDescription", temp.getDescription());
        intent.putExtra("LocalPhone", temp.getPhoneNumber());
        intent.putExtra("LocalSchedule", temp.getSchedule());
        startActivity(intent);
    }

    public void onClick(View v) {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        final int ACTIVITY_SELECT_IMAGE = 1234;
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case 1234:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePath = cursor.getString(columnIndex);
                    cursor.close();


                    SelectedImage = BitmapFactory.decodeFile(filePath);
                    View l = findViewById(R.id.bkgr);

                    ImageView _image = (ImageView) findViewById(R.id.local_management_image);

                    _image.setImageDrawable(Drawable.createFromPath(filePath));
            /* Now you have choosen image in Bitmap format in object "yourSelectedImage". You can use it in way you want! */
                }
        }

    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.local_management, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.create_offer) {
            Intent createOffer = new Intent(this, CrearOferta.class);

            // TODO: PASARLE LOS DATOS DEL LOCAL ACTUAL A LA SIGUIENTE ACTIVITY
            createOffer.putExtra("LocalID", local.getId());
            createOffer.putExtra("LocalName", local.getName());
            createOffer.putExtra("LocalAddress", local.getAddress());
            createOffer.putExtra("LocalRating", local.getRating());
            createOffer.putExtra("LocalPrice", local.getPrice());
            createOffer.putExtra("LocalImage", local.getImage());
            createOffer.putExtra("LocalCategory", local.getCategoryID());
            createOffer.putExtra("LocalCategoryName", local.getCategoryName());
            createOffer.putExtra("LocalDescription", local.getDescription());
            createOffer.putExtra("LocalPhone", local.getPhoneNumber());
            createOffer.putExtra("LocalSchedule", local.getSchedule());

            startActivity(createOffer);
            return true;
        }

        if (id == R.id.create_event) {
            Intent crearEvento = new Intent(this, CrearEvento.class);

            // TODO: PASARLE LOS DATOS DEL LOCAL ACTUAL A LA SIGUIENTE ACTIVITY
            crearEvento.putExtra("LocalID", local.getId());
            crearEvento.putExtra("LocalName", local.getName());
            crearEvento.putExtra("LocalAddress", local.getAddress());
            crearEvento.putExtra("LocalRating", local.getRating());
            crearEvento.putExtra("LocalPrice", local.getPrice());
            crearEvento.putExtra("LocalImage", local.getImage());
            crearEvento.putExtra("LocalCategory", local.getCategoryID());
            crearEvento.putExtra("LocalCategoryName", local.getCategoryName());
            crearEvento.putExtra("LocalDescription", local.getDescription());
            crearEvento.putExtra("LocalPhone", local.getPhoneNumber());
            crearEvento.putExtra("LocalSchedule", local.getSchedule());

            startActivity(crearEvento);
            return true;
        }
        else if(id == R.id.delete_event) {
            Intent deleteEvento = new Intent(this, DeleteEvent.class);
            deleteEvento.putExtra("Localid", local.getId());
            startActivity(deleteEvento);
        }
        else if(id == R.id.test_post_preferences) {
            final User u = new User("email@email.com", 6, null, "testName", "1234567890", "testSurname");
            AsyncTask at = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    try {
                        Log.d("post res" , WSController.postUserPreferences(u, 9, 2, "Algo Muy Lejos", 3));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
