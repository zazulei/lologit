package com.upv.pin.lolo;
import android.util.Log;

import com.upv.pin.BusinessLogic.Booking;
import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Comment;
import com.upv.pin.BusinessLogic.Event;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;
import com.upv.pin.BusinessLogic.Offer;
import com.upv.pin.BusinessLogic.User;
import com.upv.pin.BusinessLogic.UserPreferences;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class WSController {
    //Webservice URL
   // private static String URL = "http://192.168.1.100:8080//LoloServer2/webresources/entities.users";
   // private static String nameSpace = "http://88.20.223.252:8080/LoloServer2/webresources/";
    private static String nameSpace ="http://loloip.ddns.net/LoloServer2/webresources/";
    //private static String nameSpace ="http://192.168.1.39:8080/LoloServer2/webresources/";

    private static BufferedInputStream invokeMethod(String webmethod) {
        URL url;
        InputStream stream = null;
        BufferedInputStream res = null;
        try {
            url = new URL(nameSpace+webmethod);
            System.out.println(url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            stream = conn.getInputStream();
            res = new BufferedInputStream(stream);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return res;
    }

    public static Local getLocalID(int localId) {
        try {
            return XMLParser.getLocals(invokeMethod("entities.locals/" + localId)).get(0);
        }catch(IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LocalList getLocals(String method, String extra){
       try {
            return XMLParser.getLocals(invokeMethod("entities.locals/" + method + "/" + extra));
       }catch(IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LocalList getLocalsByFilteredSearch(String method, String keywordSelected, String categorySelected, String distanceSelected, String priceSelected, String ratingSelected){
        try {
            return XMLParser.getLocals(invokeMethod("entities.locals/findByAny/" + keywordSelected + "/" + categorySelected + "/" + ratingSelected + "/" + priceSelected));
        }catch(IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Event> getEvents(Local l){
        int id = l.getId();
        ArrayList<Event> events = new ArrayList<Event>();
        try {
            events= (ArrayList<Event>) XMLParser.getEvents(invokeMethod("entities.events/findByLocalId/"+id));
            // XMLParser.getEvents(invokeMethod("entities.events/id/"+l.getId()));
            Log.i("Events", "" + events.size());
            return events;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return events;
    }

    public static List<Offer> getOffers(Local l){
        int id = l.getId();
        ArrayList<Offer> offers = new ArrayList<Offer>();
        try {
            offers = (ArrayList<Offer>) XMLParser.getOffers(invokeMethod("entities.offers/findByLocalId/" + id));
            Log.i("Offers",""+offers.size());
            return offers;
        }catch(IOException e) {
            e.printStackTrace();
        }
        return offers;
    }

    public static List<Comment> getComments(Local l) {
        List<Comment> comments = new ArrayList<Comment>();
        try {
            comments = XMLParser.getComments(invokeMethod("entities.comments/findByLocalId/" + l.getId()));
        }catch(IOException e) {
            e.printStackTrace();
        }
        return comments;
    }

    public static User userLogin(String email, String password) {
        List<User> users = new ArrayList<User>();
        try {
            users = XMLParser.getUsers(invokeMethod("entities.users/login/"+ email + "/" + password));
        }catch(IOException e) {
            e.printStackTrace();
        }
        if(users.isEmpty()) return null;
        else return users.get(0);
    }

    public static Boss bossLogin(String email, String password) {
        List<Boss> bosses = new ArrayList<Boss>();
        try {
            bosses = XMLParser.getBosses(invokeMethod("entities.bosses/login/" + email + "/" + password));
        }catch(IOException e) {
            e.printStackTrace();
        }
        if(bosses.isEmpty()) return null;
        else return bosses.get(0);
    }

    public static LocalList getPreferencesLocals(User u) {
        LocalList locals = new LocalList();
        try {
            locals = XMLParser.getLocals(invokeMethod("entities.locals/findByPreferences/" + u.getId()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locals;
    }

    public static UserPreferences getUserPreferences(int userId) {
        UserPreferences pref = new UserPreferences();
        try {
            pref = XMLParser.getUserPreferences(invokeMethod("entities.preferences/findUserId/" + userId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pref;
    }

    public static Boss getBossById(int id) {
        Boss boss = new Boss();
        try {
            boss = XMLParser.getBosses(invokeMethod("entities.bosses/" + id)).get(0);
        }catch (IOException e ) {
            e.printStackTrace();
        }
        return boss;
    }

    public static User getUserById(int id) {
        User user = new User();
        try {
            user = XMLParser.getUsers(invokeMethod("entities.users/" + id)).get(0);
        }catch (IOException e ) {
            e.printStackTrace();
        }
        return user;
    }

    public static LocalList getLocalsByBoss(int bossId) {
        LocalList locals = new LocalList();
        try {
            locals = XMLParser.getLocals(invokeMethod("entities.locals/findByBoss/" + bossId));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locals;
    }

    public static String postComment(Comment c) throws Exception {
        invokeMethod("entities.comments/postComment/" + c.getLocalId() + "/" + c.getUserId() + "/" + c.getCommentText().replaceAll(" ", "%20"));
        return "ok";
    }

    public static String postOffer(Offer o) throws Exception {
            System.out.println(o.getDate() + " " + o.getTitle() + " " + o.getLocal().getId() + " " + o.getDescription() + " " + o.getImage());
            String image = o.getImage();
            if(image.isEmpty()) image = "http://media-cache-ak0.pinimg.com/originals/5b/88/05/5b8805b4f1d94bc3c45666cdae2f3fc4.jpg";
            invokeMethod("entities.offers/postOffer/" + o.getDate().toString().replaceAll(" ", "%20") +"/"+ o.getDescription() +"/"+ o.getLocal().getId() + "/"+ image.replaceAll("/","Hector") +"/"+ o.getTitle());
            return "ok";
    }

    public static String postEvent(Event o) throws Exception {
        String image = o.getImage();
        if(image.isEmpty()) image = "http://media-cache-ak0.pinimg.com/originals/5b/88/05/5b8805b4f1d94bc3c45666cdae2f3fc4.jpg";
        invokeMethod("entities.events/postEvent/" + o.getDate().toString().replaceAll(" ", "%20") +"/"+ o.getDescription() +"/"+ o.getLocal().getId() + "/"+ image.replaceAll("/","Hector") +"/"+ o.getTitle());
        return "ok";
    }

    public static String postBooking(Booking b) throws Exception {
        invokeMethod("entities.bookings/postBooking/" + b.getDateLong() + "/" + b.getLocalId() + "/" + b.getUserId() + "/" + b.getNumAssistants());
        return "ok";
    }

    public static String postLocalRating(Local l, int newRating) throws Exception {
        invokeMethod("entities.locals/updateRating/" + l.getId() + "/" + newRating);
        return "ok";
    }

    public static String updateUser(User u) throws Exception {
        invokeMethod("entities.users/updateUser/" + u.getId() + "/" + u.getName() + "/" + u.getSurname() + "/" + u.getImageURL() + "/" + u.getEmail() + "/" + u.getPassword() + "/" + u.getPhone());
        return "ok";
    }

    public static String updateLocal(Local l) throws Exception {
        invokeMethod("entities.locals/updateInfo/" + l.getId() + "/" + l.getName().replaceAll(" ", "%20")+ "/" + l.getDescription().replaceAll(" ", "%20") + "/"+ l.getCategoryID() + "/" + l.getAddress().replaceAll(" ", "%20") +"/" + l.getLatitude() + "/" + l.getLongitude()+"/"+ l.getPrice() + "/" + l.getRating()+"/"+ l.getNumRatings() + "/" + l.getEmail().replaceAll(" ", "%20") + "/" + l.getImage().replaceAll(" ", "%20").replaceAll("/","Hector")+"/"+ l.getPhoneNumber().replaceAll(" ", "%20"));
        return "ok";
    }

    public static String registerNewUser(User u) throws MalformedURLException {
        try {
            List<User> userList = XMLParser.getUsers(invokeMethod("entities.users/createNewUser/" + u.getName() + "/" + u.getEmail() + "/" + u.getPassword()));
            if(userList.isEmpty()) return "ok";
            else return "UsuarioExistente";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ok";
    }

    public static String postUserPreferences(User u, int categoryId, int price, String distance, int rating) throws Exception {
        URL url = new URL("http://loloip.ddns.net/LoloServer2/webresources/entities.preferences");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/xml");

        // Create the form content
        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("<preferences>" +
                "<preferenceDistance>" + distance + "</preferenceDistance>" +
                "<preferenceIdCategory>" +
                "<categoryId>" + categoryId + "</categoryId>" +
                "</preferenceIdCategory>" +
                "<preferenceIdUser>" +
                "<userId>" + u.getId() + "</userId>" +
                "</preferenceIdUser>" +
                "<preferencePrice>" + price + "</preferencePrice>" +
                "<preferenceRating>" + rating + "</preferenceRating>" +
                "</preferences>");
        writer.close();
        out.close();

        // Buffer the result into a string
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();

        conn.disconnect();
        return sb.toString();
    }

    public static void saveLocal(Local l){
        StringWriter localXML;
        try {
            localXML = XMLParser.localToXML(l);
            invokeMethod("entities.locals/POST/"+localXML);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String post(String urlMethod, StringWriter XMLObject)throws Exception{
        URL url = new URL(nameSpace+urlMethod);

        //connection type and format
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/xml");

        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write(XMLObject.toString());


        // Buffer the result into a string
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();

        conn.disconnect();
        return sb.toString();

    }

    public static String deleteOffer(int offerId) {
        invokeMethod("entities.offers/deleteOffer/" + offerId);
        return "ok";
    }

    public static String deleteEvent(int eventId) {
        invokeMethod("entities.events/deleteEvent/" + eventId);
        return "ok";
    }
}


