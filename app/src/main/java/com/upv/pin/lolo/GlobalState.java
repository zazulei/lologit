package com.upv.pin.lolo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.User;

/**
 * Created by mifercre on 12/11/14.
 */
public class GlobalState extends Application {


    public boolean userIsLogedIn() {
        SharedPreferences preferences = getSharedPreferences("LoloSP", Context.MODE_PRIVATE);
        return preferences.getBoolean("userIsLogedIn", false);
    }

    public boolean bossIsLogedIn() {
        SharedPreferences preferences = getSharedPreferences("LoloSP", Context.MODE_PRIVATE);
        return preferences.getBoolean("bossIsLogedIn", false);
    }

    public void logoutUser() {
        SharedPreferences preferences = this.getSharedPreferences("LoloSP", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("userIsLogedIn", false);

        editor.commit();

        Toast.makeText(GlobalState.this, "User loged out", Toast.LENGTH_SHORT).show();
    }

    public void logoutBoss() {
        SharedPreferences preferences = this.getSharedPreferences("LoloSP", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("bossIsLogedIn", false);

        editor.commit();

        Toast.makeText(GlobalState.this, "Boss loged out", Toast.LENGTH_SHORT).show();
    }

    public User getUser() {
        SharedPreferences preferences = getSharedPreferences("LoloSP", Context.MODE_PRIVATE);

        User user = new User();
        user.setId(preferences.getInt("usuarioId", -1));
        user.setEmail(preferences.getString("usuarioEmail", "no email"));
        user.setImageURL(preferences.getString("usuarioImageURL", "no image"));
        user.setName(preferences.getString("usuarioName", "no name"));
        user.setPhone(preferences.getString("usuarioPhone", "no phone"));
        user.setSurname(preferences.getString("usuarioSurname", "no surname"));
        user.setPassword(preferences.getString("usuarioPassword", "no password"));

        return user;
    }

    public void setUser(User user) {
        SharedPreferences preferences = this.getSharedPreferences("LoloSP", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("userIsLogedIn", true);
        editor.putInt("usuarioId", user.getId());
        editor.putString("usuarioEmail", user.getEmail());
        editor.putString("usuarioImageURL", user.getImageURL());
        editor.putString("usuarioName", user.getName());
        editor.putString("usuarioPhone", user.getPhone());
        editor.putString("usuarioSurname", user.getSurname());
        editor.putString("usuarioPassword", user.getPassword());

        editor.commit();
    }

    public Boss getBoss() {
        SharedPreferences preferences = getSharedPreferences("LoloSP", Context.MODE_PRIVATE);

        Boss boss = new Boss();
        boss.setId(preferences.getInt("bossId", -1));
        boss.setEmail(preferences.getString("bossEmail", "no email"));
        boss.setImageURL(preferences.getString("bossImageURL", "no image"));
        boss.setName(preferences.getString("bossName", "no name"));
        boss.setPhone(preferences.getString("bossPhone", "no phone"));
        boss.setSurname(preferences.getString("bossSurname", "no surname"));
        boss.setPassword(preferences.getString("bossPassword", "no password"));
        boss.setIdLocal(preferences.getInt("bossLocalId", -1));

        return boss;
    }

    public void setBoss(Boss boss) {
        Context mContext = getApplicationContext();
        SharedPreferences preferences = mContext.getSharedPreferences("LoloSP", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("bossIsLogedIn", true);
        editor.putInt("bossId", boss.getId());
        editor.putString("bossEmail", boss.getEmail());
        editor.putString("bossImageURL", boss.getImageURL());
        editor.putString("bossName", boss.getName());
        editor.putString("bossPhone", boss.getPhone());
        editor.putString("bossSurname", boss.getSurname());
        editor.putString("bossPassword", boss.getPassword());
        editor.putInt("bossLocalId", boss.getIdLocal());

        editor.commit();
    }
}
