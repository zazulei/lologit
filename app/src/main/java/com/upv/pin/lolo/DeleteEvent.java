package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Event;
import com.upv.pin.BusinessLogic.ExpandableListAdapter;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class DeleteEvent extends Activity {


    String eventTitle;
    Event _event, e;
    ArrayList<Event> eventList;
    String [] values;
    private ListView listView;
    private ArrayAdapter<String> adapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_event);
        listView = (ListView)findViewById(R.id.delete_event_listView);
        new AsyncTask<String,Void, ArrayList<Event>>(){
            @Override
            protected ArrayList<Event> doInBackground(String... strings) {
                Local l = new Local();
                l.setId(getIntent().getIntExtra("Localid", 0));
                eventList = WSController.getEvents(l);
                return eventList;
            }
            protected void onPostExecute(ArrayList<Event> eventList) {
                Log.d("onPostExecute Local", "entering");
                loadedFinish();

            }
        }.execute();
    }

    private void loadedFinish(){
        values = new String[eventList.size()];
        for (int i = 0; i < eventList.size(); i++){
            _event = eventList.get(i);
            eventTitle = _event.getTitle();
            values [i] = eventTitle;
        }
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        System.out.println(eventList.size()+"");
        if(eventList.size() == 0) {
            Toast.makeText(this, "Oops ningún evento fue encontrado", Toast.LENGTH_LONG).show();
            this.finish();
        }
        System.out.println(listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                delete(values[position]);

            }
        });


    }

    private void delete ( String name){
        Iterator<Event> it = eventList.iterator();



        e=it.next();
        boolean bol = true;
        while(it.hasNext() && bol){
            if(e.getTitle().equals(name))
                bol = false;
        }

        new AsyncTask<String,Void, String>(){
            @Override
            protected String doInBackground(String... strings) {
                return WSController.deleteEvent(e.getId());
            }
            protected void onPostExecute(String s) {
                Log.d("onPostExecute Local", "entering");

                Intent i = new Intent(DeleteEvent.this, DeleteEvent.class);
                startActivity(i);

            }
        }.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.delete_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
