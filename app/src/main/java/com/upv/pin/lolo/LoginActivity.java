package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.LocalList;
import com.upv.pin.BusinessLogic.User;


public class LoginActivity extends Activity implements View.OnClickListener {

    private Button loginAsUser;
    private Button loginAsBoss;

    private EditText editEmail;
    private EditText editPassword;

    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginAsUser = (Button) findViewById(R.id.buttonLoginUser);
        loginAsUser.setOnClickListener(this);
        loginAsBoss = (Button) findViewById(R.id.buttonLoginBoss);
        loginAsBoss.setOnClickListener(this);

        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.buttonLoginUser:
                loginAsUser();
                break;
            case R.id.buttonLoginBoss:
                loginAsBoss();
                break;
        }
    }

    private void loginAsUser() {
        email = editEmail.getText().toString();
        password = editPassword.getText().toString();

        if(email != null && !email.isEmpty() && password != null && !password.isEmpty()) {
            LoginUserTask task = new LoginUserTask();
            task.execute(email,password);
        } else {
            Toast.makeText(this, "No has indicado todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void loginAsBoss() {
        email = editEmail.getText().toString();
        password = editPassword.getText().toString();

        if(email != null && !email.isEmpty() && password != null && !password.isEmpty()) {
            LoginBossTask task = new LoginBossTask();
            task.execute(email,password);
        } else {
            Toast.makeText(this, "No has indicado todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private class LoginUserTask extends AsyncTask<String, Void, User> {

        @Override
        protected User doInBackground(String... params) {
            return WSController.userLogin(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(User result) {
            if(result == null) {
                Toast.makeText(LoginActivity.this, "Email o contraseña incorrectos", Toast.LENGTH_SHORT).show();
            } else {
                GlobalState state = (GlobalState) getApplicationContext();
                state.setUser(result);
                Toast.makeText(LoginActivity.this, "Loged in!", Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();
            }
        }
    }

    private class LoginBossTask extends AsyncTask<String, Void, Boss> {

        @Override
        protected Boss doInBackground(String... params) {
            return WSController.bossLogin(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(Boss result) {
            if(result == null) {
                Toast.makeText(LoginActivity.this, "Email o contraseña incorrectos", Toast.LENGTH_SHORT).show();
            } else {
                GlobalState state = (GlobalState) getApplicationContext();
                state.setBoss(result);
                Toast.makeText(LoginActivity.this, "Loged in!", Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();
            }
        }
    }
}
