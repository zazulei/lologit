package com.upv.pin.lolo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ShareActionProvider;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Comment;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;
import com.upv.pin.BusinessLogic.User;

import java.util.List;


public class LocalProfileActivity extends Activity {

    String id, address, name, rating, price, image, category, desc, phone, sched;
    int boss_id;
    Boss boss;
    Local _local;
    User _user;
    String email;
    Intent intent;
    private double longitud, latitud;

    List<Comment> _listComment;
    LinearLayout _last10Comment;

    public final static String EXTRA_UBICACION = "com.google.example.lolo.UBIC";
    public final static String EXTRA_LOCNAME = "com.google.example.lolo.LOCALNAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_profile);

        _local = new Local();
        _local.setId(getIntent().getIntExtra("LocalID", 0));
        _local.setName(getIntent().getStringExtra("LocalName"));
        _local.setAddress(getIntent().getStringExtra("LocalAddress"));
        _local.setRating(getIntent().getIntExtra("LocalRating", 0));
        _local.setNumRatings(getIntent().getIntExtra("LocalNumRatings", 0));
        _local.setPrice(getIntent().getIntExtra("LocalPrice", 0));
        _local.setImage(getIntent().getStringExtra("LocalImage"));
        _local.setCategoryID(getIntent().getIntExtra("LocalCategory", 0));
        _local.setCategoryName(getIntent().getStringExtra("LocalCategoryName"));
        _local.setDescription(getIntent().getStringExtra("LocalDescription"));
        _local.setPhoneNumber(getIntent().getStringExtra("LocalPhone"));
        _local.setSchedule(getIntent().getStringExtra("LocalSchedule"));
        _local.setLongitude(getIntent().getDoubleExtra("LocalLongitud", 0));
        _local.setLatitude((getIntent().getDoubleExtra("LocalLatitud", 0)));
        _local.setMaxAssistants((getIntent().getIntExtra("MaxAssistants", 0)));
        _local.setBossId((getIntent().getIntExtra("BossId", 0)));

        setEmailByBossId();

        address = _local.getAddress();
        phone = _local.getPhoneNumber();
        sched = _local.getSchedule();
        longitud = _local.getLongitude();
        latitud = _local.getLatitude();

        GetComments getComments = new GetComments();
        getComments.execute();

        Spinner spinnerLike = (Spinner) findViewById(R.id.value_like);
        ArrayAdapter<CharSequence> adapterLike = ArrayAdapter.createFromResource(this,
                R.array.local_profiles_value_like, android.R.layout.simple_spinner_item);
        adapterLike.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLike.setAdapter(adapterLike);

        _last10Comment = (LinearLayout) findViewById(R.id.last10Comment);

        LinearLayout formComent = (LinearLayout) findViewById(R.id.form_coment);
        formComent.setVisibility(LinearLayout.GONE);
    }

    private void checkFormComment() {
        //PASAR AL OTRO MENTODO CHANGE
        GlobalState state = (GlobalState) getApplicationContext();

        if( state.userIsLogedIn() ) {
            _user = state.getUser();

            TextView nameUserComent = (TextView) findViewById(R.id.name_user_coment);
            nameUserComent.setText(" " + _user.getName());

            LinearLayout formComent = (LinearLayout) findViewById(R.id.form_coment);
            formComent.setVisibility(existMyReview() ? LinearLayout.GONE: LinearLayout.VISIBLE);

            Log.d("Envio comentario", "---- Login");

        } else {
            LinearLayout formComent = (LinearLayout) findViewById(R.id.form_coment);
            formComent.setVisibility(LinearLayout.GONE);

            Log.d("Envio comentario", "----No - Login");
        }

    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public boolean existMyReview() {
        for( int i = 0; i < _listComment.size(); i++ ) {
            if( _listComment.get(i).getUserId() == _user.getId() ) {
                return true;
            }
        }
        return false;
    }

    private void loadPage(String method) {
        /*
        if(method != null && method.length() != 0) {
            downloadXmlTask = new DownloadXmlTask();
            if(method.equals("findBySearch")) {
                keywordSelected = getIntent().getStringExtra("keyword");
                distanceSelected = getIntent().getStringExtra("distance");
                categorySelected = getIntent().getStringExtra("category");
                priceSelected = getIntent().getIntExtra("price", -1) + "";
                ratingSelected = getIntent().getIntExtra("rating", -1) +"";
                downloadXmlTask.execute(method);
            }
            else if(method.equals("findByCategory")) {
                category = getIntent().getStringExtra("category");
                downloadXmlTask.execute(method);
            }
            else if(method.equals("findByPreferences")) {
                downloadXmlTask.execute(method);
            }
            //poner el nombre de la categoria en el actionbar
            ActionBar ab = getActionBar();
            ab.setTitle(category);
        } else {
            Toast.makeText(this, "Metodo incorrecto", Toast.LENGTH_SHORT).show();
        }
        */
    }

    public void openOffer(View v){
        Intent i = new Intent(this, OfferListActivity.class);
        i.putExtra("localid", _local.getId());
        startActivity(i);
    }

    public void openEvent(View v){
        Intent i = new Intent(this, EventListActivity.class);
        i.putExtra("localid", _local.getId());
        startActivity(i);
    }


    public void sendComent(View v) {
       Log.d("Envio comentario", "ok");

       String commentText = ((EditText) findViewById(R.id.text_comentary)).getText().toString();
       Spinner spinnerLike = (Spinner) findViewById(R.id.value_like);
       if( commentText.length() <= 0 || spinnerLike.getSelectedItemPosition() == 0 ) {
           Toast.makeText(this, "Faltan datos para añadir el comentario", Toast.LENGTH_SHORT).show();
           return;
       }

        LinearLayout formComent = (LinearLayout) findViewById(R.id.form_coment);
        formComent.setVisibility(LinearLayout.GONE);

       new AsyncTask<Void, Void, Void>() {
           @Override
           protected Void doInBackground(Void... objects) {
               String commentText = ((EditText) findViewById(R.id.text_comentary)).getText().toString();
               int userId = _user.getId();
               int localId = _local.getId();
               Comment comment = new Comment(commentText, userId, localId);
               Spinner spinnerLike = (Spinner) findViewById(R.id.value_like);

               try {
                   WSController.postComment(comment);
                   WSController.postLocalRating(_local, spinnerLike.getSelectedItemPosition());
               } catch (Exception e) {
                   e.printStackTrace();
               }

               GetComments getComments = new GetComments();
               getComments.execute();

               return null;
           }
       }.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.local_profile, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);

        ShareActionProvider myShareActionProvider = (ShareActionProvider) item.getActionProvider();
        Intent myIntent = new Intent();
        myIntent.setAction(Intent.ACTION_SEND);
        myIntent.putExtra(Intent.EXTRA_TEXT, "¡Échale un vistazo a " + _local.getName() + " de la categoría " + _local.getCategoryName() + " en LoLo!");
        myIntent.setType("text/plain");

        myShareActionProvider.setShareIntent(myIntent);

        TextView _price = (TextView) findViewById(R.id.price);
        TextView _rate = (TextView) findViewById(R.id.rating);
        int fontColor = getResources().getColor(R.color.yellow_price);

        String birchFontPath = "fonts/birch-std.ttf";
        Typeface tfLocalList = Typeface.createFromAsset(getAssets(), birchFontPath);
        _rate.setTypeface(tfLocalList);
        _price.setTypeface(tfLocalList);

        _rate.setTextColor(fontColor);
        _price.setTextColor(fontColor);
        unSwitch(_local.getPrice());
        mostrarDatos();
        return true;
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_item_share) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    public void mostrarDatos() {
        //ImageView _image = (ImageView) findViewById(R.id.local_profile_image);
        cambiarIcono();

        ImageView _image = (ImageView) findViewById(R.id.local_profile_image);
        TextView _name = (TextView) findViewById(R.id.local_profile_name);
        TextView _desc = (TextView) findViewById(R.id.local_profile_description);
        TextView _price = (TextView) findViewById(R.id.price);
        TextView _rate = (TextView) findViewById(R.id.rating);
        //Button _call = (Button) findViewById(R.id.call_button);
        System.out.println(_local.getImage() + " <-");
        if(_local.getImage() != null &&_local.getImage().contains("/storage/emulated/")) _image.setImageDrawable(Drawable.createFromPath(_local.getImage()));
        else Picasso.with(this).load(_local.getImage()).into(_image);

        _name.setText(_local.getName()+"");
        _price.setText(price+"");

        double rate = ((double)(_local.getRating())/(double)(_local.getNumRatings()));
        double p = Math.pow(10d, 1);
        double rating = Math.round(rate * p)/p;
        _rate.setText(rating + "");
        //_image.setBackground(Drawable.createFromPath(_local.getImage()));
        _desc.setText(_local.getDescription()+"\n" + _local.getAddress()+" " + _local.getLatitude());

        //_call.setText(phone+"");


    }

    public void onBooking(View v){

        GlobalState state = (GlobalState) getApplicationContext();
        if(state.userIsLogedIn()) {
            Intent book = new Intent(this, BookingActivity.class);
            book.putExtra("LocalImage",_local.getImage());
            book.putExtra("LocalName",_local.getName());
            book.putExtra("BossEmail",email);
            //book.putExtra("LocalAddress",_local.getAddress());
            book.putExtra("LocalID",_local.getId());
            book.putExtra("MaxAssistants",_local.getMaxAssistants());
            startActivity(book);
        } else {
            Intent login = new Intent(this, LoginActivity.class);
            startActivity(login);
        }
    }

    public void unSwitch(int precio){

        switch(precio){
            case 1: price = "€";
                break;
            case 2: price = "€€";
                break;
            case 3: price = "€€€";
                break;
            case 4: price = "€€€€";
                break;
            case 5: price = "€€€€€";
                break;
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void cambiarIcono(){
        ImageView _icono = (ImageView) findViewById(R.id.icono_perfil);
        System.out.println(_local.getCategoryName()+"<----NOMBRE");
        if(_local.getCategoryName().equals("Bares")) _icono.setBackground(getResources().getDrawable(R.drawable.beer));
        else if(_local.getCategoryName().equals("Discotecas")) _icono.setBackground(getResources().getDrawable(R.drawable.disco));
            //else if(_local.getCategoryName().equals("Cines")) _icono.setBackground(getResources().getDrawable(R.drawable.));
        else if(_local.getCategoryName().equals("Restaurantes")) _icono.setBackground(getResources().getDrawable(R.drawable.beer));
        else if(_local.getCategoryName().equals("Pubs")) _icono.setBackground(getResources().getDrawable(R.drawable.pubs));
        else if(_local.getCategoryName().equals("Hoteles")) _icono.setBackground(getResources().getDrawable(R.drawable.bares));
        else if(_local.getCategoryName().equals("Teatros")) _icono.setBackground(getResources().getDrawable(R.drawable.bares));
    }

    /*
    Se invoca al pulsar el boton Mapa y muestra la posición del local
     */
    public void openMap(View view){
        double ubicacion_local[] = new double[2];
        final Intent mapIntent = new Intent(this, MapActivity.class);
        ubicacion_local[0] = latitud;
        ubicacion_local[1] = longitud;
        String name = _local.getName();
        mapIntent.putExtra(EXTRA_UBICACION,ubicacion_local);
        mapIntent.putExtra(EXTRA_LOCNAME, name);
        startActivity(mapIntent);
    }

    public void setEmailByBossId(){
        boss_id = _local.getBossId();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    boss = WSController.getBossById(boss_id);
                    email = boss.getEmail();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public void changeUI(List<Comment> result){
        _listComment = result;
        checkFormComment();
        int count = 0;
        Log.d("Comentarios 1", "" + result.size() );

        while(count < result.size() && count <= 10) {
            Comment c = result.get((result.size() - 1 - count));
            Log.d("Comentarios Text", "" + c.getCommentText() );
            //_last10Comment

            LinearLayout lName = new LinearLayout(this);
            TextView tName = new TextView(this);
            tName.setText(""+c.getUserName());
            lName.addView(tName);
            _last10Comment.addView(lName);

            LinearLayout lComment = new LinearLayout(this);
            TextView tComment = new TextView(this);
            tComment.setText("   "+c.getCommentText());
            lComment.addView(tComment);
            _last10Comment.addView(lComment);
            //_last10Comment

            /*
                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent">
                            <TextView
                                android:layout_width="wrap_content"
                                android:layout_height="wrap_content" />
                        </LinearLayout>
                        <LinearLayout
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content">
                            <TextView
                                android:layout_width=""
                                android:layout_height="" />
                            </LinearLayout>
             */

            count++;
        }

        Log.d("Comentarios 2", "" + result.size() );
    }

    private class GetComments extends AsyncTask<String, Void,List<Comment>> {

        @Override
        protected List<Comment> doInBackground(String... methods) {
            return WSController.getComments(_local);
        }

        @Override
        protected void onPostExecute(List<Comment> result) {
            Log.d("onPostExecute Local", "entering");
            changeUI(result);
        }
    }
}
