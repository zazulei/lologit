package com.upv.pin.lolo;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import com.upv.pin.BusinessLogic.Event;
import com.upv.pin.BusinessLogic.ExpandableListAdapter;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.Offer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class EventListActivity extends Activity {

    ExpandableListAdapter listAdapterEvent;
    ExpandableListView expListEvent;
    List<String> listDataHeaderEvent;
    HashMap<String, List<String>> listDataChildEvent;
    String eventTitle, eventSchedule, eventDesc;
    Event _event;
    ArrayList<Event> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_list);

        new AsyncTask<String,Void, ArrayList<Event>>(){
            @Override
            protected ArrayList<Event> doInBackground(String... strings) {
                Local l = new Local();
                l.setId(getIntent().getIntExtra("localid", 0));
                eventList = WSController.getEvents(l);
                return eventList;
            }
            protected void onPostExecute(ArrayList<Event> eventList) {
                Log.d("onPostExecute Local", "entering");
                for (int i = 0; i < eventList.size(); i++){
                    _event = eventList.get(i);

                    eventTitle = _event.getTitle();
                    eventSchedule = "Evento válido hasta: " + _event.getDate();
                    eventDesc = _event.getDescription();

                    // get the listview
                    expListEvent = (ExpandableListView) findViewById(R.id.event_list);
                    expListEvent.setScrollContainer(false);
                    // preparing list data
                    prepareListData();

                    prepareListAdapter();
                }
            }
        }.execute();
    }

    private void prepareListAdapter(){
        listAdapterEvent = new ExpandableListAdapter(this, listDataHeaderEvent, listDataChildEvent);

        // setting list adapter
        expListEvent.setAdapter(listAdapterEvent);

        System.out.println("EVENTOS: " + eventTitle + " " + eventDesc + " " + eventSchedule);
    }

    private void prepareListData() {
        listDataHeaderEvent = new ArrayList<String>();
        listDataChildEvent = new HashMap<String, List<String>>();

        // Adding head data
        listDataHeaderEvent.add(eventTitle);

        // Adding child data
        List<String> expListContent = new ArrayList<String>();
        expListContent.add(eventSchedule);
        expListContent.add(eventDesc);

        listDataChildEvent.put(listDataHeaderEvent.get(0), expListContent); // Header, Child data
    }
}
