package com.upv.pin.lolo;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;


public class ManagementListActivity extends Activity implements AdapterView.OnItemClickListener{

    private DownloadXmlTask2 downloadXmlTask;
    private ListView listView;
    private ListViewAdapter adapter;
    private LocalList listaLocales = new LocalList();



    private GlobalState state;
    private Boss boss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management_list);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        state = (GlobalState) getApplicationContext();
        boss = state.getBoss();


        listView = (ListView)findViewById(R.id.management_listView);
        ConnectivityManager cm =
                (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if(isConnected) loadPage();
        else {
            Toast.makeText(this, "Necesitas conexion a internet", Toast.LENGTH_SHORT).show();
        }
        getActionBar().setTitle("Gestión de locales");

    }

    private void loadPage() {

        String id = ""+boss.getId();
        downloadXmlTask = new DownloadXmlTask2();
        downloadXmlTask.execute(id);

    }

    private void loadedFinish(LocalList result){
        listaLocales = result;
        adapter = new ListViewAdapter(this, this.getLayoutInflater(), result);
        System.out.println(result.size()+"");
        if(result.size() == 0) {
            Toast.makeText(this, "Oops ningún local fue encontrado", Toast.LENGTH_LONG).show();
            this.finish();
        }
        System.out.println(listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Local temp = listaLocales.get(position); //Local seleccionado
        System.out.println(temp.getName());
        System.out.println(temp.getImage());

        Intent intent = new Intent(this, LocalManagementActivity.class);
        intent.putExtra("LocalID", temp.getId());
        intent.putExtra("LocalName", temp.getName());
        intent.putExtra("Email",temp.getEmail());
        intent.putExtra("LocalAddress", temp.getAddress());
        intent.putExtra("LocalRating", temp.getRating());
        intent.putExtra("LocalRatingNumber", temp.getNumRatings());
        intent.putExtra("LocalPrice", temp.getPrice());
        intent.putExtra("LocalImage", temp.getImage());
        intent.putExtra("LocalCategory", temp.getCategoryID());
        intent.putExtra("LocalCategoryName", temp.getCategoryName());
        intent.putExtra("LocalDescription", temp.getDescription());
        intent.putExtra("LocalPhone", temp.getPhoneNumber());
        intent.putExtra("LocalSchedule", temp.getSchedule());
        intent.putExtra("LocalLongitud", temp.getLongitude());
        intent.putExtra("LocalLatitud", temp.getLatitude());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_search:
                Intent searchIntent = new Intent(this, SearchActivity.class);
                startActivity(searchIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    protected void onStop() {
        super.onStop();
        downloadXmlTask.cancel(false);
    }*/

    private class DownloadXmlTask2 extends AsyncTask<String, Void,LocalList > {

        @Override
        protected LocalList doInBackground(String... id) {
            //Log.d("URL", urls[0].toString());
            int i = Integer.parseInt(id[0]);

            return WSController.getLocalsByBoss(i);
        }

        @Override
        protected void onPostExecute(LocalList result) {
            Log.d("onPostExecute Local", "entering");
            loadedFinish(result);
        }
    }
}
