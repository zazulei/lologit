package com.upv.pin.lolo;

public class Constants {
    public static final int LOCALS_BLOCK_SIZE = 10;

    public static final String URL_LOCALES = "http://158.42.57.47:8080/LoloServer2/webresources/entities.locals";
    public static final String URL_LOCALES_BY_SEARCH = "http://158.42.57.47:8080/LoloServer2/webresources/entities.locals";
    public static final String URL_LOCALES_BY_CATEGORY = "http://158.42.57.47:8080/LoloServer2/webresources/entities.locals";

    public static final int LOGIN = 11;
    public static final int LOGOUT_USER = 12;
    public static final int LOGOUT_BOSS = 13;
    public static final int PERFIL = 14;
    public static final int GESTION_LOCAL = 15;
    public static final int LOCALS_BY_USER_PREFERENCES = 16;
    public static final int REGISTER = 17;


}
