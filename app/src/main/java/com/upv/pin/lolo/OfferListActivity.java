package com.upv.pin.lolo;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import com.upv.pin.BusinessLogic.Event;
import com.upv.pin.BusinessLogic.ExpandableListAdapter;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.Offer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class OfferListActivity extends Activity {

    ExpandableListAdapter listAdapterOffer;
    ExpandableListView expListOffer;
    List<String> listDataHeaderOffer;
    HashMap<String, List<String>> listDataChildOffer;
    String offerTitle, offerSchedule, offerDesc;
    Offer _offer;
    ArrayList<Offer> offerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_list);

        new AsyncTask<String,Void, ArrayList<Offer>>(){
            @Override
            protected ArrayList<Offer> doInBackground(String... strings) {
                Local l = new Local();
                l.setId(getIntent().getIntExtra("localid", 0));
                offerList = (ArrayList<Offer>) WSController.getOffers(l);
                return offerList;
            }
            @Override
            protected void onPostExecute(ArrayList<Offer> offerList) {
                Log.d("onPostExecute Local", "entering");
                for (int i = 0; i < offerList.size(); i++){
                    _offer = offerList.get(i);

                    offerTitle = _offer.getTitle();
                    offerSchedule = "Oferta válida hasta: " + _offer.getDate();
                    offerDesc = _offer.getDescription();

                    // get the listview
                    expListOffer = (ExpandableListView) findViewById(R.id.offer_list);
                    expListOffer.setScrollContainer(false);
                    // preparing list data
                    prepareListData();

                    prepareListAdapter();
                }
            }
        }.execute();
    }

    private void prepareListAdapter(){
        listAdapterOffer = new ExpandableListAdapter(this, listDataHeaderOffer, listDataChildOffer);

        // setting list adapter
        expListOffer.setAdapter(listAdapterOffer);

        System.out.println("OFERTAS: " + offerTitle + " " + offerDesc + " " + offerSchedule);
    }

    private void prepareListData() {
        listDataHeaderOffer = new ArrayList<String>();
        listDataChildOffer = new HashMap<String, List<String>>();

        List<String> expListContent = new ArrayList<String>();

        // Adding head data
        listDataHeaderOffer.add(offerTitle);

        // Adding child data
        expListContent.add(offerSchedule);
        expListContent.add(offerDesc);

        listDataChildOffer.put(listDataHeaderOffer.get(0), expListContent); // Header, Child data
    }
}
