package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;
import com.upv.pin.BusinessLogic.User;


public class PerfilManagementActivity extends Activity {

    private TextView perfilId;
    private TextView perfilName;
    private ListView listView;
    private User user;
    private EditText _name,_email, _surname, _phone, _password, _newpassword, _newpassword2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_management);

        //perfilId = (TextView) findViewById(R.id.perfil_id);
        //perfilName = (TextView) findViewById(R.id.perfil_name);

        GlobalState state = (GlobalState) getApplicationContext();
        user = state.getUser();//preferences.getInt("usuarioId", -1);
        Boss boss = state.getBoss();//preferences.getInt("bossId", -1);

        /*if(state.userIsLogedIn() && user != null) {
            perfilId.setText(user.getId() + "");
            perfilName.setText(user.getName());
        } else if(state.bossIsLogedIn() && boss != null) {
            perfilId.setText(boss.getId() + "");
            perfilName.setText(boss.getName());
        }*/

        _name = (EditText) findViewById(R.id.edit_user_name);
        _surname = (EditText) findViewById(R.id.edit_surname);
        _email = (EditText) findViewById(R.id.edit_user_email);
        _phone = (EditText) findViewById(R.id.edit_user_phone);
        _password = (EditText) findViewById(R.id.edit_password);
        _newpassword = (EditText) findViewById(R.id.edit_password_new);
        _newpassword2 = (EditText) findViewById(R.id.edit_password2);


        _name.setText(user.getName());
        _surname.setText(user.getSurname());
        _email.setText(user.getEmail());
        _phone.setText(user.getPhone());

        listView = (ListView)findViewById(R.id.management_listView);


    }

    public void updateUser(View v){

        if (!_name.getText().toString().equals(user.getName()) || !_email.getText().toString().equals(user.getEmail()) ||
                !_surname.getText().toString().equals(user.getSurname()) || _password.getText().length() != 0);
        {
            ConnectivityManager cm =
                    (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (!isConnected)
                Toast.makeText(this, "Necesitas conexion a internet", Toast.LENGTH_SHORT).show();
            else {

                if (_name.getText().length() == 0)
                    Toast.makeText(this, "El nombre esta vacio", Toast.LENGTH_SHORT).show();
                else if (_email.getText().length() == 0)
                    Toast.makeText(this, "El email esta vacio", Toast.LENGTH_SHORT).show();
                else if (_surname.getText().length() == 0)
                    Toast.makeText(this, "La contraseña esta vacia", Toast.LENGTH_SHORT).show();
                else if (!_email.getText().toString().contains("@"))
                    Toast.makeText(this, "El email no es correcto", Toast.LENGTH_SHORT).show();
                else {
                    //User user1 = new User();
                    user.setName(_name.getText().toString().replace(" ", "%20"));
                    user.setSurname(_surname.getText().toString().replace(" ", "%20"));
                    user.setEmail(_email.getText().toString().replace(" ", "%20"));
                    user.setPhone(_phone.getText().toString().replace(" ", "%20"));
                    user.setImageURL(user.getImageURL().replace(" ", "%20"));

                    System.out.println((_password.getText().toString()));
                    if (user.getPassword().equals(_password.getText().toString())) {
                        if (_newpassword.getText().toString().equals(_newpassword2.getText().toString())) {
                            user.setPassword(_newpassword.getText().toString().replace(" ", "%20"));
                            Update up = new Update();
                            up.execute(user);
                        } else
                            Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    } else if (!_password.getText().toString().equals(""))
                        Toast.makeText(this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();

                    if (_password.getText().toString().equals("")) {
                        Update up = new Update();
                        up.execute(user);
                    }
                }
            }


        }
    }



    private class Update extends AsyncTask<User, Void, String> {

        @Override
        protected String doInBackground(User... params) {
            try {
                return WSController.updateUser(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {

            GlobalState state = (GlobalState) getApplicationContext();
            state.setUser(user);
            Toast.makeText(PerfilManagementActivity.this, "Perfil actualizado", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(PerfilManagementActivity.this, HomeActivity.class);
            startActivity(i);
        }
    }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.perfil_management, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
