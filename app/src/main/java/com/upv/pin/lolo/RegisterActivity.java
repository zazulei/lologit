package com.upv.pin.lolo;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.User;

import java.net.MalformedURLException;


public class RegisterActivity extends Activity implements View.OnClickListener {

    private EditText editName;
    private EditText editEmail;
    private EditText editContraseña;
    private EditText editRepetir;

    private Button buttonOk;
    private Button buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editName = (EditText) findViewById(R.id.editTextName);
        editEmail = (EditText) findViewById(R.id.editTextEmail);
        editContraseña = (EditText) findViewById(R.id.editTextContraseña);
        editRepetir = (EditText) findViewById(R.id.editTextRepite);

        buttonOk = (Button) findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(this);
        buttonReset = (Button) findViewById(R.id.buttonReset);
        buttonReset.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonOk:
                registerUser();
                break;
            case R.id.buttonReset:
                editName.setText("");
                editEmail.setText("");
                editContraseña.setText("");
                editRepetir.setText("");
                break;
        }
    }

    private void registerUser() {
        if(editName.getText().length() == 0) Toast.makeText(this, "El nombre esta vacio", Toast.LENGTH_SHORT).show();
        else if(editEmail.getText().length() == 0) Toast.makeText(this, "El email esta vacio", Toast.LENGTH_SHORT).show();
        else if(editContraseña.getText().length() == 0) Toast.makeText(this, "La contraseña esta vacia", Toast.LENGTH_SHORT).show();
        else if(editRepetir.getText().length() == 0) Toast.makeText(this, "Repite la contraseña", Toast.LENGTH_SHORT).show();
        else if(!editRepetir.getText().toString().equals(editContraseña.getText().toString())) Toast.makeText(this, "La contraseña no coincide", Toast.LENGTH_SHORT).show();
        else if(!editEmail.getText().toString().contains("@")) Toast.makeText(this, "El email no es correcto", Toast.LENGTH_SHORT).show();
        else {
            RegisterTask task = new RegisterTask();
            task.execute();
        }
    }

    private class RegisterTask extends AsyncTask<String, String,String> {

        @Override
        protected String doInBackground(String... methods) {
            User u = new User();
            u.setName(editName.getText().toString().replace(" ", "%20"));
            u.setEmail(editEmail.getText().toString().trim());
            u.setPassword(editContraseña.getText().toString().trim());
            String res = null;
            try {
                res = WSController.registerNewUser(u);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("result", result);
            if(result.equals("ok")) {
                Toast.makeText(RegisterActivity.this, "Usuario creado", Toast.LENGTH_SHORT).show();
                finish();
            }
            else if(result.equals("UsuarioExistente")) {
                Toast.makeText(RegisterActivity.this, "El usuario ya existe anteriormente", Toast.LENGTH_SHORT).show();
                editEmail.setText("");
            }
        }
    }
}
