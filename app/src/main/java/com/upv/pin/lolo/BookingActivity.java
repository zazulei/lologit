package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.upv.pin.BusinessLogic.Booking;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.User;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class BookingActivity extends Activity {
    Booking booking;
    TimePicker timePicker;
    DatePicker datePicker;
    NumberPicker assistants;
    int _year, _month, _day, _hour, _minutes, maxAssistants, minAssistants, advanceHour, advanceData;
    long data = 0;
    ImageView bookingImage;
    TextView local_name;
    Local _local;
    String mail_address, mail_body;
    GlobalState state;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        state = (GlobalState) getApplicationContext();
        user = state.getUser();

        bookingImage = (ImageView) findViewById(R.id.booking_image);
        local_name = (TextView) findViewById(R.id.booking_local_name);

        _local = new Local();
        _local.setImage(getIntent().getStringExtra("LocalImage"));
        _local.setName(getIntent().getStringExtra("LocalName"));
        _local.setAddress(getIntent().getStringExtra("LocalAddress"));
        _local.setId(getIntent().getIntExtra("LocalID",0));
        _local.setMaxAssistants(getIntent().getIntExtra("MaxAssistants",0));

        mail_address = getIntent().getStringExtra("BossEmail");
        System.out.println("Correo booking: "+mail_address+"...");

        Picasso.with(this).load(_local.getImage()).into(bookingImage);
        local_name.setText(_local.getName());

        maxAssistants = _local.getMaxAssistants();
        minAssistants = 1;
        advanceHour = 1;
        advanceData = 1;

        setPeopleMinMaxValues(minAssistants,maxAssistants);

        timePicker = (TimePicker) findViewById(R.id.hora_reserva);
        datePicker = (DatePicker) findViewById(R.id.fecha_reserva);
        assistants = (NumberPicker) findViewById(R.id.personas_reserva);

        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

        Calendar calendar = Calendar.getInstance();
        _year = calendar.get(Calendar.YEAR);
        System.out.println("Year: "+_year);

        _month = calendar.get(Calendar.MONTH)+1;
        System.out.println("Month: "+_month);

        _day = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println("Day: "+_day);

        _hour = calendar.get(Calendar.HOUR_OF_DAY);
        System.out.println("Hour: "+_hour);

        _minutes = calendar.get(Calendar.MINUTE);
        System.out.println("Minutes: "+_minutes);

    }

    public void finish(View v){
        String text = "Hola, amigo";

        if(correctDate()) {
            text = "Abriendo correo...";
            sendMail();
        }
        else text = "Error al crear la reserva, fecha incorrecta";

        Toast.makeText(this,text,Toast.LENGTH_SHORT).show();

    }

    public boolean correctDate(){
        int year = datePicker.getYear();
        System.out.println("Year Pick: "+year);

        int month = datePicker.getMonth()+1;
        System.out.println("Month Pick: "+month);

        int day = datePicker.getDayOfMonth();
        System.out.println("Day Pick: "+day);

        int hour = timePicker.getCurrentHour();
        System.out.println("Hour Pick: "+hour);

        //int minutes = timePicker.getCurrentMinute();

        if(year < _year) return false;
        else if (year == _year) {
            if (month < _month) return false;
            else if(month == _month){
                if (day < _day) return false;
                else if (day ==_day){
                    if(hour <= (_hour)+advanceHour) return false;
                    else return true;
                }
            }
        }
        return true;
    }

    public void setPeopleMinMaxValues(int min, int max){
        NumberPicker num = (NumberPicker) findViewById(R.id.personas_reserva);
        if (min < 1) num.setMinValue(1);
        else num.setMinValue(min);

        if (max < 1) num.setMaxValue(10);
        else num.setMaxValue(max);
    }

    public void getTextBody(){
        mail_body = "Hola, mi nombre es "+user.getName()+" y quiero reservar en su local "+_local.getName()+" para "+assistants.getValue()+" personas.";
        //Nombre y Apellido serán sustituidos por el nombre completo y otros datos del usuario al implementar la versión del servidor
    }

    public void sendMail(){
        data = dataToMilis();
        booking = new Booking(user.getId(),_local.getId(),assistants.getValue(),data);
        bookingToServer();

        Intent mail_intent = new Intent(Intent.ACTION_SEND);
        getTextBody();
        mail_intent.setType("text/mail");
        mail_intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""+mail_address});
        mail_intent.putExtra(Intent.EXTRA_SUBJECT, "Reserva");
        mail_intent.putExtra(Intent.EXTRA_TEXT, mail_body);
        startActivity(mail_intent);
    }

    public void bookingToServer(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    WSController.postBooking(booking);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public long dataToMilis(){
        long dataInMilis = 0;
        try{
            String date_str = timePicker.getCurrentHour()+"-"+datePicker.getDayOfMonth()+"-"+datePicker.getMonth()+"-"+datePicker.getYear();
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh-dd-MM-yyyy");
            Date parsedData = dateFormat.parse(date_str);
            dataInMilis = parsedData.getTime();
        } catch(ParseException pe){}
        return dataInMilis;
    }
}
