package com.upv.pin.lolo;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.os.AsyncTask;
import com.upv.pin.BusinessLogic.*;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


public class LocalListActivity extends Activity implements AdapterView.OnItemClickListener {

    //private ArrayList<HashMap <String, String> > list;
    private DownloadXmlTask downloadXmlTask;
    private ListView listView;
    private ListViewAdapter adapter;
    private LocalList listaLocales = new LocalList();
    private String keywordSelected,categorySelected,distanceSelected,priceSelected,ratingSelected;
    private String category;
    private String method;

    private GlobalState state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_list);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        state = (GlobalState) getApplicationContext();

        method = getIntent().getStringExtra("method");

        listView = (ListView)findViewById(R.id.locals_listView);
        ConnectivityManager cm =
                (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if(isConnected) loadPage(method);
        else {
            Toast.makeText(this, "Necesitas conexion a internet", Toast.LENGTH_SHORT).show();
        }

    }

    private void loadPage(String method) {
        if(method != null && method.length() != 0) {
            downloadXmlTask = new DownloadXmlTask();
            if(method.equals("findBySearch")) {
                keywordSelected = getIntent().getStringExtra("keyword");
                distanceSelected = getIntent().getStringExtra("distance");
                categorySelected = getIntent().getStringExtra("category");
                priceSelected = getIntent().getIntExtra("price", -1) + "";
                ratingSelected = getIntent().getIntExtra("rating", -1) +"";
                downloadXmlTask.execute(method);
            }
            else if(method.equals("findByCategory")) {
                category = getIntent().getStringExtra("category");
                downloadXmlTask.execute(method);
            }
            else if(method.equals("findByPreferences")) {
                downloadXmlTask.execute(method);
            }
            //poner el nombre de la categoria en el actionbar
            ActionBar ab = getActionBar();
            ab.setTitle(category);
        } else {
            Toast.makeText(this, "Metodo incorrecto", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadedFinish(LocalList result){
        listaLocales = result;
        adapter = new ListViewAdapter(this, this.getLayoutInflater(), result);
        System.out.println(result.size()+"");
        if(result.size() == 0) {
            Toast.makeText(this, "Oops ningún local fue encontrado", Toast.LENGTH_LONG).show();
            this.finish();
        }
        System.out.println(listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Local temp = listaLocales.get(position); //Local seleccionado
        System.out.println(temp.getName());

        Intent intent = new Intent(this, LocalProfileActivity.class);
        intent.putExtra("LocalID", temp.getId());
        intent.putExtra("LocalName", temp.getName());
        intent.putExtra("LocalAddress", temp.getAddress());
        intent.putExtra("LocalRating", temp.getRating());
        intent.putExtra("LocalNumRatings", temp.getNumRatings());
        intent.putExtra("LocalPrice", temp.getPrice());
        intent.putExtra("LocalImage", temp.getImage());
        intent.putExtra("LocalCategory", temp.getCategoryID());
        intent.putExtra("LocalCategoryName", temp.getCategoryName());
        intent.putExtra("LocalDescription", temp.getDescription());
        intent.putExtra("LocalPhone", temp.getPhoneNumber());
        intent.putExtra("LocalSchedule", temp.getSchedule());
        intent.putExtra("LocalLongitud", temp.getLongitude());
        intent.putExtra("LocalLatitud", temp.getLatitude());
        intent.putExtra("MaxAssistants", temp.getMaxAssistants());
        intent.putExtra("BossId", temp.getBossId());
        intent.putExtra("BossEmail", temp.getBossEmail());

        System.out.println("Correo localList: "+temp.getBossEmail()+"...");

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_search:
                Intent searchIntent = new Intent(this, SearchActivity.class);
                startActivity(searchIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DownloadXmlTask extends AsyncTask<String, Void,LocalList > {

        @Override
        protected LocalList doInBackground(String... methods) {
            //Log.d("URL", urls[0].toString());
            if(methods[0].equals("findBySearch")) return WSController.getLocalsByFilteredSearch(methods[0], keywordSelected, categorySelected, distanceSelected, priceSelected, ratingSelected);
            else if(methods[0].equals("findByCategory")) return WSController.getLocals(methods[0], category);
            else if(methods[0].equals("findByPreferences")) return WSController.getPreferencesLocals(state.getUser());
            else return null;
        }

        @Override
        protected void onPostExecute(LocalList result) {
            Log.d("onPostExecute Local", "entering");
            loadedFinish(result);
        }
    }
}