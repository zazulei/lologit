package com.upv.pin.lolo;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Activity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener{

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private double[] ubic;
    private String name;
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationClient mLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        //recibir la ubicacion desde LocalProfileActivity
        Intent perfilIntent = getIntent();
        ubic = perfilIntent.getDoubleArrayExtra(LocalProfileActivity.EXTRA_UBICACION);
        name = perfilIntent.getStringExtra(LocalProfileActivity.EXTRA_LOCNAME);
        setUpMapIfNeeded();
        getActionBar().setDisplayHomeAsUpEnabled(true);
        mLocationClient = new LocationClient(this, this, this);
       // GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());


    }

    private void mockLocation(){
        mLocationClient.setMockMode(true);
        String provider = "flp";
        double mockLat = 39.482671;
        double mockLong = -0.347384;
        float accu = 3.0f;
        Location mockLoc = new Location(provider);
        mockLoc.setLatitude(mockLat);
        mockLoc.setLongitude(mockLong);
        mockLoc.setAccuracy(accu);
        mLocationClient.setMockLocation(mockLoc);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    //user location
    @Override
    protected void onStart() {
        Log.d("onStart","OnStart executed");
        super.onStart();
        mLocationClient.connect();

    }

    @Override
    protected void onStop() {
        mLocationClient.disconnect();
        super.onStop();
    }



    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        Fragment myMapFrag = getFragmentManager().findFragmentById(R.id.map);
        myMapFrag.getActivity().getActionBar().setTitle(name);
        LatLng latlang = new LatLng(ubic[0], ubic[1]);
        mMap.addMarker(new MarkerOptions().position(latlang).title(name));
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(latlang, 16.0f) );
        //user position
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //User location

    //definir ventana de error
    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
                switch (resultCode){
                    case Activity.RESULT_OK:
                        Log.d("tag1","Problem resolved by Play services");
                        break;
                    default:
                        Log.d("tag2", "No resolution");
                        break;
                }
            default:
                Log.d("tag3","Unknown request code");
                break;
        }
    }

    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            return true;
            // Google Play services was not available for some reason.
            // resultCode holds the error code.
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }

    //comprobar localizacion activada
    private boolean isLocationEnabled(){
        LocationManager lman = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return(lman.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    //Llamado por Location Services cuando la petición para conectar el cliente ha acaba con exito
    @Override
    public void onConnected(Bundle bundle) {

        if(servicesConnected()) {
            // Coger la posicion
            if(isLocationEnabled()) {
                Log.d("tagConectadsoPlayServ", "Conectado a Play Services");
                mockLocation();
            }
            else Toast.makeText(this,"Lolo necesita acceder a su ubicación. Por favor, active el servicio de ubicación",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(this, "Desconectado. Por favor, re-connectar.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            //showErrorDialog(connectionResult.getErrorCode());
            Toast.makeText(this,"Error al conectarse a Play Services",Toast.LENGTH_SHORT).show();
        }
    }
}
