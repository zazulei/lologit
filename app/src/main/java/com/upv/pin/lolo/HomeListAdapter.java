package com.upv.pin.lolo;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Nikolay on 27/09/2014.
 */
public class HomeListAdapter extends ArrayAdapter<String> {
    private final Activity activity;
    private final String[] nombres;
    private final int[] imagenes;

    public HomeListAdapter(Activity context, String[] nombres, int[] img){
        super(context,R.layout.list_single, nombres);
        activity = context;
        this.nombres = nombres;
        this.imagenes = img;

    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single, null, true);
        TextView nomLocal = (TextView) rowView.findViewById(R.id.txt);
        // cargar la fuente del texto
        nomLocal.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/bern.ttf"));
        ImageView icono = (ImageView) rowView.findViewById(R.id.img);
        nomLocal.setText(nombres[position]);
        icono.setImageResource(imagenes[position]);
        return rowView;
    }


}
