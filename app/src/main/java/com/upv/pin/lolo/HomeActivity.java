package com.upv.pin.lolo;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.upv.pin.BusinessLogic.Boss;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.User;
import com.upv.pin.chat.ui.activities.ChatActivity;
import com.upv.pin.chat.ui.activities.SplashActivity;


public class HomeActivity extends Activity {
    ListView lista;
    //TextView club_text_view = (TextView) findViewById(R.id.string_club);
    String[] locales = {"Bares", "Discotecas", "Pubs", "Restaurantes", "Teatros","Hoteles"};
    int[] imagenes = {R.drawable.bares, R.drawable.discoteca, R.drawable.pubs, R.drawable.restaurante, R.drawable.teatro, R.drawable.hotel};

    private GlobalState state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        state = (GlobalState) getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.invalidateOptionsMenu();

        HomeListAdapter adapter = new HomeListAdapter(HomeActivity.this, locales, imagenes);
        final Intent intento = new Intent(this, LocalListActivity.class);
        lista = (ListView) findViewById(R.id.home_list_view);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            intento.putExtra("method", "findByCategory");
            System.out.println(locales[position]);
            intento.putExtra("category", locales[position]);
            startActivity(intento);
            }
            });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        if(state.userIsLogedIn()) {
            menu.add(0, Constants.LOGOUT_USER,0, "Logout user");
            menu.add(0, Constants.PERFIL,0, "Perfil");
            menu.add(0, Constants.LOCALS_BY_USER_PREFERENCES, 0, "Locales según prefs.");
            menu.getItem(1).setVisible(true);
        }
        else if(state.bossIsLogedIn()) {
            menu.add(0, Constants.LOGOUT_BOSS,0, "Logout boss");
            menu.add(0, Constants.PERFIL,0, "Perfil");
            menu.add(0, Constants.GESTION_LOCAL, 0, "Gestión de local");
        }
        else {
            menu.add(0, Constants.LOGIN,0, "Login");
            menu.add(0, Constants.REGISTER,0, "Register");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       int id = item.getItemId();
        switch (id){
            case R.id.action_search:
                Intent searchIntent = new Intent(this, SearchActivity.class);
                startActivity(searchIntent);
                return true;
            case R.id.action_chat:
                Intent chatIntent = new Intent(this, SplashActivity.class);
                User user = ((GlobalState)getApplicationContext()).getUser();
                chatIntent.putExtra("userEmail", user.getEmail());
                chatIntent.putExtra("userPassword", user.getPassword());
                startActivity(chatIntent);
                return true;
            case Constants.REGISTER:
                Intent registerIntent = new Intent(this, RegisterActivity.class);
                startActivity(registerIntent);
                return true;
            case Constants.LOGIN:
                Intent home = new Intent(this, LoginActivity.class);
                startActivity(home);
                break;
            case Constants.LOGOUT_USER:
                state.logoutUser();
                this.invalidateOptionsMenu();
                break;
            case Constants.LOGOUT_BOSS:
                state.logoutBoss();
                this.invalidateOptionsMenu();
                break;
            case Constants.PERFIL:
                Intent perfilManagement= new Intent(this, PerfilManagementActivity.class);
                startActivity(perfilManagement);
                break;
            case Constants.LOCALS_BY_USER_PREFERENCES:
                Intent intento = new Intent(this, LocalListActivity.class);
                intento.putExtra("method", "findByPreferences");
                startActivity(intento);
                break;
            case Constants.GESTION_LOCAL:
                Intent localManagement= new Intent(this, ManagementListActivity.class);
                startActivity(localManagement);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
