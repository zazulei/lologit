package com.upv.pin.lolo;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Niki on 07/10/2014.
 * Actividad para cargar la fuente externa para el texto de los locales
 */
public class ExternalFontsActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Font path
        String fontPath = "fonts/bern.ttf";
        // text view label
        TextView txtGhost = (TextView) findViewById(R.id.txt);
        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        // Applying font
        txtGhost.setTypeface(tf);
    }
}
