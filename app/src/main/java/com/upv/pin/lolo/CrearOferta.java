package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pkmmte.view.CircularImageView;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.Offer;
import com.upv.pin.lolo.R;

import java.io.Console;
import java.util.Date;

public class CrearOferta extends Activity {

    Local _local;
    private Bitmap SelectedImage;
    private String filePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_oferta);

        //int idLocal = getIntent().getIntExtra("localID", 1);
        //local = new Local();
        //local.setId(idLocal);

        _local = new Local();
        _local.setId(getIntent().getIntExtra("LocalID", 0));
        _local.setName(getIntent().getStringExtra("LocalName"));
        _local.setAddress(getIntent().getStringExtra("LocalAddress"));
        _local.setRating(getIntent().getIntExtra("LocalRating", 0));
        _local.setPrice(getIntent().getIntExtra("LocalPrice", 0));
        _local.setImage(getIntent().getStringExtra("LocalImage"));
        _local.setCategoryID(getIntent().getIntExtra("LocalCategory", 0));
        _local.setCategoryName(getIntent().getStringExtra("LocalCategoryName"));
        _local.setDescription(getIntent().getStringExtra("LocalDescription"));
        _local.setPhoneNumber(getIntent().getStringExtra("LocalPhone"));
        _local.setSchedule(getIntent().getStringExtra("LocalSchedule"));

        final Button button = (Button) findViewById(R.id.send_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createOffer();
            }
        });
    }

    public void onClick(View v) {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        final int ACTIVITY_SELECT_IMAGE = 1234;
        startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case 1234:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePath = cursor.getString(columnIndex);
                    cursor.close();


                    SelectedImage = BitmapFactory.decodeFile(filePath);
                    View l = findViewById(R.id.bkgr);

                    CircularImageView _image = (CircularImageView) findViewById(R.id.local_management_image);

                    _image.setImageDrawable(Drawable.createFromPath(filePath));
            /* Now you have choosen image in Bitmap format in object "yourSelectedImage". You can use it in way you want! */
                }
        }

    };


    public void createOffer() {
        final EditText name = (EditText) findViewById(R.id.name);
        final EditText description = (EditText) findViewById(R.id.description);
        final DatePicker dataPicker = (DatePicker) findViewById(R.id.date);

        long dateTime = dataPicker.getCalendarView().getDate();
        Date date = new Date(dateTime);

        Log.d("Name", name.getText().toString());
        Log.d("Descripcion", description.getText().toString());
        Log.d("Fecha hasta", date.toString());

        if( name.getText().toString().length() == 0 ||  description.getText().toString().length() == 0  ) {
            Context context = getApplicationContext();
            CharSequence text = "No has rellenado toda la informacion.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            final Offer offer = new Offer(date, description.getText().toString(), 0, _local, name.getText().toString(), filePath);
            CircularImageView _image = (CircularImageView) findViewById(R.id.local_management_image);

            Log.d("", "AAAAA - " + _image.toString());

            //TODO: Enviar a offerta
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        WSController.postOffer(offer);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute();

            Intent intent = new Intent(this, LocalProfileActivity.class);
            intent.putExtra("LocalID", _local.getId());
            intent.putExtra("LocalName", _local.getName());
            intent.putExtra("LocalAddress", _local.getAddress());
            intent.putExtra("LocalRating", _local.getRating());
            intent.putExtra("LocalPrice", _local.getPrice());
            intent.putExtra("LocalImage", _local.getImage());
            intent.putExtra("LocalCategory", _local.getCategoryID());
            intent.putExtra("LocalCategoryName", _local.getCategoryName());
            intent.putExtra("LocalDescription", _local.getDescription());
            intent.putExtra("LocalPhone", _local.getPhoneNumber());
            intent.putExtra("LocalSchedule", _local.getSchedule());
            startActivity(intent);
        }

    }

}
