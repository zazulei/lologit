package com.upv.pin.lolo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.upv.pin.BusinessLogic.Local;
import com.upv.pin.BusinessLogic.LocalList;

public class ListViewAdapter extends BaseAdapter{

    Activity activity;
    private Local tempValues;
    private LocalList data;
    private static LayoutInflater inflater = null;

    public ListViewAdapter(Activity activity, LayoutInflater in,LocalList result){
        super();
        this.activity = activity;
        data = result;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        public TextView localName;
        public TextView localSchedule;
        public TextView localRate;
        public TextView localPrice;
        public ImageView localImage;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        //LayoutInflater inflater = activity.getLayoutInflater();

        tempValues = (Local) data.get(position);
        if(view == null){

            view = inflater.inflate(R.layout.column_row, null);
            holder = new ViewHolder();

            holder.localName = (TextView) view.findViewById(R.id.textLocalName);
            holder.localSchedule = (TextView) view.findViewById(R.id.textLocalSchedule);
            holder.localRate = (TextView) view.findViewById(R.id.textLocalRate);
            holder.localPrice = (TextView) view.findViewById(R.id.textLocalPrice);
            holder.localImage = (ImageView) view.findViewById(R.id.imageLocal);

            int fontColor = activity.getResources().getColor(R.color.yellow_price);

            String birchFontPath = "fonts/birch-std.ttf";

            Typeface tfLocalList = Typeface.createFromAsset(activity.getAssets(), birchFontPath);

            holder.localRate.setTypeface(tfLocalList);
            holder.localPrice.setTypeface(tfLocalList);

            holder.localRate.setTextColor(fontColor);
            holder.localPrice.setTextColor(fontColor);

            view.setTag(holder);
        }else{

            holder=(ViewHolder) view.getTag();
            Log.d("tempValues", tempValues.getName());

        }

        int color;
        if(position % 2 == 0){
            color = activity.getResources().getColor(R.color.green);
            view.setBackgroundColor(color);
        }else{
            color = activity.getResources().getColor(R.color.medium_green);
            view.setBackgroundColor(color);
        }
        if(tempValues != null){
            holder.localName.setText(tempValues.getName());
            holder.localSchedule.setText(tempValues.getSchedule());

            if(tempValues.getImage().contains("/storage/emulated/")) holder.localImage.setImageDrawable(Drawable.createFromPath(tempValues.getImage()));
            else Picasso.with(activity).load(tempValues.getImage()).into(holder.localImage);

            switch (tempValues.getPrice()){
                case 1:
                    holder.localRate.setText("€");
                    break;
                case 2:
                    holder.localRate.setText("€€");
                    break;
                case 3:
                    holder.localRate.setText("€€€");
                    break;
                case 4:
                    holder.localRate.setText("€€€€");
                    break;
                case 5:
                    holder.localRate.setText("€€€€€");
                    break;
            }
            double rate = ((double)(tempValues.getRating())/(double)(tempValues.getNumRatings()));
            double p = Math.pow(10d, 1);
            double result = Math.round(rate * p)/p;

            holder.localPrice.setText(rate + "");
            /**switch (tempValues.getRating()){
                case 1:
                    holder.localPrice.setText("1/5");
                    break;
                case 2:
                    holder.localPrice.setText("2/5");
                    break;
                case 3:
                    holder.localPrice.setText("3/5");
                    break;
                case 4:
                    holder.localPrice.setText("4/5");
                    break;
                case 5:
                    holder.localPrice.setText("5/5");
                    break;
            }**/
        }

        return view;
    }
}
