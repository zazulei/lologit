package com.upv.pin.chat.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.quickblox.core.QBCallbackImpl;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.quickblox.users.QBUsers;
import com.upv.pin.chat.ApplicationSingleton;
import com.upv.pin.lolo.R;

import org.jivesoftware.smack.SmackException;

import java.util.List;

public class SplashActivity extends Activity {

    private static final String APP_ID = "16856";
    private static final String AUTH_KEY = "Y45uJKsq9tHNRPh";
    private static final String AUTH_SECRET = "j2VBLFuPXAGA6sz";

    private String USER_PASSWORD = "";
    //
    //private static final String USER_LOGIN = "";
    //private static final String USER_PASSWORD = "bobbobbob";

    static final int AUTO_PRESENCE_INTERVAL_IN_SECONDS = 30;

    private QBChatService chatService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Init Chat
        //
        QBChatService.setDebugEnabled(true);
        QBSettings.getInstance().fastConfigInit(APP_ID, AUTH_KEY, AUTH_SECRET);
        if (!QBChatService.isInitialized()) {
            QBChatService.init(this);
        }
        chatService = QBChatService.getInstance();


        // create QB user
        //
        final QBUser user = new QBUser();

        final String USER_LOGIN = getIntent().getStringExtra("userEmail");
        USER_PASSWORD = getIntent().getStringExtra("userPassword");

        user.setLogin(USER_LOGIN);
        user.setPassword(USER_PASSWORD+"test");
        user.setEmail(USER_LOGIN);

        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>(){
            @Override
            public void onSuccess(QBSession session, Bundle args) {
                Log.i("SESIONSUCCESS", "session " + session);

                QBUsers.signUp(user, new QBEntityCallbackImpl<QBUser>() {
                    @Override
                    public void onSuccess(QBUser user, Bundle args) {
                        Log.i("SINGUPSUCCESS", "user " + user + " has been singed up");
                        user.setPassword(USER_PASSWORD + "test");
                        //isSignedUp = true;
                        signIn(user);
                    }

                    @Override
                    public void onError(List<String> errors) {
                        Log.i("SINGUPERRORS", errors.get(0));

                        signIn(user);
                    }
                });
            }

            @Override
            public void onError(List<String> errors) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this);
                dialog.setMessage("create session errors: " + errors).create().show();
            }
        });

    }

    public void signIn(QBUser user) {
        QBUsers.signIn(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                Log.i("SINGINSUCCESS", "user " + user + " has been singed in");
                user.setPassword(USER_PASSWORD+"test");
                // save current user
                //
                //user.setId(session.getUserId());
                ApplicationSingleton.setCurrentUser(user);

                // login to Chat
                //
                loginToChat(user);
            }

            @Override
            public void onError(List<String> errors) {
                Log.i("SINGINERRORS", errors.get(0));
            }
        });
    }

    private void loginToChat(final QBUser user){

        chatService.login(user, new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {

                // Start sending presences
                //
                try {
                    chatService.startAutoSendPresence(AUTO_PRESENCE_INTERVAL_IN_SECONDS);
                } catch (SmackException.NotLoggedInException e) {
                    e.printStackTrace();
                }

                // go to Dialogs screen
                //
                Intent intent = new Intent(SplashActivity.this, DialogsActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onError(List errors) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SplashActivity.this);
                dialog.setMessage("chat login errors: " + errors).create().show();
            }
        });
    }
}