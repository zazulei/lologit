package com.upv.pin.chat;

import android.app.Application;
import android.util.Log;

import com.quickblox.chat.model.QBDialog;
import com.quickblox.users.model.QBUser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationSingleton {

    private static QBUser currentUser;

    private static Map<Integer, QBUser> dialogsUsers = new HashMap<Integer, QBUser>();

    /*@Override
    public void onCreate() {
        super.onCreate();
    }*/


    public static QBUser getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(QBUser _currentUser) {
        currentUser = _currentUser;
    }

    public static  Map<Integer, QBUser> getDialogsUsers() {
        return dialogsUsers;
    }

    public static void setDialogsUsers(List<QBUser> setUsers) {
        dialogsUsers.clear();

        for (QBUser user : setUsers) {
            dialogsUsers.put(user.getId(), user);
        }
    }

    public static void addDialogsUsers(List<QBUser> newUsers) {
        for (QBUser user : newUsers) {
            dialogsUsers.put(user.getId(), user);
        }
    }

    public static Integer getOpponentIDForPrivateDialog(QBDialog dialog){
        Integer opponentID = -1;
        for(Integer userID : dialog.getOccupants()){
            if(!userID.equals(getCurrentUser().getId())){
                opponentID = userID;
                break;
            }
        }
        return opponentID;
    }
}
